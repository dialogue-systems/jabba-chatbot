// Configure the ACE Editor
var editor = ace.edit("editor");
editor.setTheme("ace/theme/tomorrow_night");
editor.getSession().setMode("ace/mode/java");

// Add code sample to the IDE
addSampleCode();

// Set to false in the next sprint
var sendIdeMessage = true;

// Endpoint used to redirect messages to the compieler service
const compile_endpoint = '/compile';

// Endpoint used to check the credit in JDoodle API
const status_endpoint = '/credit_spent';

// Endpoint used to explain compiler messages in natural language
const code_analysis_endpoint = '/code_analysis';

function addSampleCode() {
    var phrases = [
        'Let\'s chat and learn Java!',
        'Let\'s learn Java together!',
        'Say hey to Jabba & let\'s learn Java!',
        'Let\'s chat and write Java code!'
    ]
    var randomPhrase = phrases[Math.floor(Math.random() * phrases.length)];
    sampleCode = 'public class Main {\n' +
        '\tpublic static void main(String[] args) {\n' +
        '\t\tSystem.out.println("' + randomPhrase + '");\n' +
        '\t}\n}';
    editor.setValue(sampleCode, 1);
}

const executeCode = async () => {
    var loader = '<div class="lds-ring"><div></div><div></div><div></div><div></div></div>';
    var code = editor.getValue();
    var stdin = document.querySelector('#stdin-window > input').value;
    var runButton = document.querySelector('.run-button');
    
    runButton.innerHTML = loader;
    displayOutput('Executing...');
    stdout = document.getElementById('stdout-window');
    
    if (stdout.clientHeight == '0') {
        showStdoutWindow()
    }

    if (code === 'credit-spent') {
        checkStatus();
        return;
    }

    request_body = {
        user_id: senderId,
        stdin: stdin,
        script: code,
        language: "java"
    };

    const response = await fetch(compile_endpoint, {
        method: 'POST',
        body: JSON.stringify(request_body),
        headers: {
        'Content-Type': 'application/json'
        }
    });

    if (response.status == 200) {
        const myJson = await response.json();
        var output = myJson.output.replace(/^\s+|\s+$/g, '');
        displayOutput(output);
        submitCode();
    } else {
        displayOutput('Compiler is not responding');
    }

    runButton.innerHTML = 'RUN ▶';
}

function displayOutput(output) {
    consoleWindow = document.querySelector('#stdout-window > p');
    output_html = output.replace(/\n/g, '<br>');
    consoleWindow.innerHTML = output_html;
    if (output.includes('error')) {
        consoleWindow.style.color = 'red';
    } else {
        consoleWindow.style.color = '#fff';
    }
    codeAnalysis(output)
}

const checkStatus = async () => {
    code = editor.getValue()

    const response = await fetch(status_endpoint, {
        method: 'POST',
        headers: {
        'Content-Type': 'application/json'
        }
    });

    const myJson = await response.json();
    document.querySelector('.run-button').innerHTML = 'RUN ▶';
    displayOutput("credits used: " + myJson.used); 
}

function showStdoutWindow() {
    stdout = document.getElementById('stdout-window');
    stdin = document.getElementById('stdin-window');
    aceEditor = document.getElementById('editor');
    errorExplainer = document.getElementById('code-analysis');
    stdin.style.height = 100;
    stdout.style.height = 100;
    if (stdout.clientHeight == '0') {
        stdin.style.display = 'none';
        stdout.style.display = 'block';
        errorExplainer.style.display = 'inline';
        aceEditor.style.height = 'calc(100% - 135px)';
    } else if (stdout.clientHeight == 100 && stdin.style.display == 'block') {
        stdin.style.display = 'none';
        stdout.style.display = 'block';
        errorExplainer.style.display = 'inline';
    } else {
        stdout.style.height = 0;
        stdin.style.height = 0;
        errorExplainer.style.display = 'none';
        aceEditor.style.height = 'calc(100% - 35px)'
    }
}

function showStdinWindow() {
    stdout = document.getElementById('stdout-window');
    stdin = document.getElementById('stdin-window');
    aceEditor = document.getElementById('editor');
    errorExplainer = document.getElementById('code-analysis');
    stdout.style.height = 100;
    stdin.style.height = 100;
    if (stdin.clientHeight == '0') {
        stdout.style.display = 'none';
        errorExplainer.style.display = 'none';
        stdin.style.display = 'block';
        aceEditor.style.height = 'calc(100% - 135px)';
    } else if (stdin.clientHeight == 100 && stdout.style.display == 'block') {
        stdout.style.display = 'none';
        errorExplainer.style.display = 'none';
        stdin.style.display = 'block';
    } else {
        stdin.style.height = 0;
        stdout.style.height = 0;
        aceEditor.style.height = 'calc(100% - 35px)'
    }
}

function showErrorExplainer() {
    errorExplainer = document.getElementById('code-analysis');
    errorExplainer.style.display = 'inline';
}

// Switching between pages - only used for evaluation
function switchPage(sPage, dPage) {
    var sourcePage = document.querySelector('#' + sPage);
    var destinationPage = document.querySelector('#' + dPage);
    destinationPage.scrollIntoView({block: "start", behavior: "smooth"}, false);

    sourcePage.style.opacity = '0';
    setTimeout(function(){ 
        sourcePage.style.opacity = '1';
    }, 1000);
}

const codeAnalysis = async (compiler_message) => {
    request_body = {
        stdout: compiler_message
    };

    const response = await fetch(code_analysis_endpoint, {
        method: 'POST',
        body: JSON.stringify(request_body),
        headers: {
        'Content-Type': 'application/json'
        }
    });

    const myJson = await response.json();
    ca_tooltip = document.querySelector("#code-analysis > .tooltiptext");
    ca_tooltip.innerHTML = myJson.message;
}