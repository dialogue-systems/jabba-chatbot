// Generate unique id for each UI
var senderId = Math.random().toString(36).substr(2, 16);

// Delay for bot responses in milliseconds
var utteranceDelay = 700;

// Delay for uploading a task to ide
var taskDelay = 1000;

// Rasa specific button payloads
var buttonPayloads = {};

// Endpoint used to redirect messages to the chatbot service
const chat_endpoint = '/chat';

// Endpoint used to cache data from the text editor
const ide_data_endpoint = '/ide_data';

// Handles text input
function enterMessage(e) {
    text = e.value;
    if (
        event.key != 'Backspace' && 
        text.length == 1 && 
        String.fromCharCode(event.keyCode).match(/(\w|\s)/g)
    ) {
        addChatLoader('user');
        scrollDownChat();
    }
    if (event.key == 'Backspace' && text.length == 2)
        removeChatLoader('user');
    if (event.key === 'Enter') {
        if (e.value != "") {
            removeChatLoader('user');
            displayMessage(e.value, 'user');
            postMessage(e.value)
            e.value = "";
            scrollDownChat();
        }
    }
}

// Sends message to the chat service (rasa api) and displays the response
const postMessage = async (text) => {
    submitCode();
    request_body = {
        sender: senderId,
        message: text
    };

    const response = await fetch(chat_endpoint, {
        method: 'POST',
        body: JSON.stringify(request_body),
        headers: {
        'Content-Type': 'application/json'
        }
    });

    if (response.status == 200) {
        const myJson = await response.json();
        displayData(myJson);
    } else {
        addChatLoader('bot');
        setTimeout(function(){
            removeChatLoader('bot');
            displayMessage('Chatbot is not responding', 'bot');
        }, utteranceDelay);
    }
}

// Sends a PUT request to cache data from the text editor IDE
const submitCode = async () => {
    request_body = {
        user_id: senderId,
        script: editor.getValue(),
        stdin: document.querySelector('#stdin-window > input').value,
        stdout: document.querySelector('#stdout-window > p').innerHTML
    };

    const response = await fetch(ide_data_endpoint, {
        method: 'PUT',
        body: JSON.stringify(request_body),
        headers: {
        'Content-Type': 'application/json'
        }
    });
}

// Adds data to chat window
function displayData(data) {
    addChatLoader('bot');
    data.forEach(function (entry, i) {
        setTimeout(function(){
            removeChatLoader('bot');
            displayBotMessage(entry)
            if (data.length != (i + 1)) {
                addChatLoader('bot');
            }
            scrollDownChat();
        }, (i + 1) * utteranceDelay)});
}

function displayMessage(utterance, user) {
    removeButtons();
    var chat = document.querySelector('#chat-window');
    chat.innerHTML += `<div class="message-box ${user}-message">${utterance}</div>`;
    scrollDownChat();
}

// Adds bot messages to the chat window.
// Data can be buttons or text (images not handled)
function displayBotMessage(data) {
    var chat = document.getElementById("chat-window");

    // Display messages
    if (data.text) {
        // Replace newlines and [SEP] with <br>
        var message = data.text.replace(/\\n/g, '<br>');
        message = message.replaceAll(' [SEP] ', '<br>');

        // check for links [text](url)
        let elements = message.match(/\[.*?\)/g);
        if( elements != null && elements.length > 0) {
            for(el of elements){
                let txt = el.match(/\[(.*?)\]/)[1]; //get only the txt
                let url = el.match(/\((.*?)\)/)[1]; //get only the link
                message = message.replace(el,'<a href="'+url+'" target="_blank">'+txt+'</a>')
            }
        }
        displayMessage(message, 'bot');
    }
    
    // Display buttons
    if (data.buttons) {
        data.buttons.forEach(button => {
            addChatButton(button.title);
            if (button.payload)
                buttonPayloads[button.title] = button.payload;
        });
        enableChatButtons();
    }

    // Display images
    if (data.image) {
        var imageTag = '<img src="' + data.image +'">';
        displayMessage(imageTag, 'bot');
    }
    
    if (data.custom) {
        ideContent = document.querySelector('.ide-content');
        loader = document.querySelector('.task-loader');
        ideContent.style.opacity = '0.9';
        loader.style.display = 'block';
        setTimeout(function(){
            editor.setValue(data.custom.template, 1);
            ideContent.style.opacity = '1';
            loader.style.display = 'none';
        }, 1000);
    }
}

function disableChatButtons() {
    var chat_buttons = document.querySelectorAll('.chat-buttons');
    chat_buttons.forEach(button => button.style.pointerEvents = 'none');
}

function enableChatButtons() {
    var chat_buttons = document.querySelectorAll('.chat-buttons');
    chat_buttons.forEach(button => button.style.pointerEvents = 'all');
}

function addChatButton(title) {
    var chat_buttons = document.querySelector('.chat-buttons');
    buttonHTML = '<button onclick="chatButtonClicked(this)">' + title + "</button>";
    chat_buttons.innerHTML += buttonHTML;
    button = document.querySelector('.chat-buttons > button:last-child');
    chat_buttons.style.opacity = '1';
    chat_buttons.style.visibility = 'visible';
    scrollDownChat();
}

function chatButtonClicked(e) {
    var title = e.innerHTML;
    disableChatButtons();
    if (title in buttonPayloads) {
        var payload = buttonPayloads[e.innerHTML];
        postMessage(payload);
    } else {
        postMessage(title);
    }
    displayMessage(title, 'user');
}

function removeButtons() {
    buttonPayloads = {};
    buttons = document.querySelectorAll('.chat-buttons > button');
    buttons.forEach(button => {
        button.style.opacity = '0';
        button.style.visibility = 'hidden';
        setTimeout(function(){ 
            button.remove();
        }, 700);
    });
}

function addChatLoader(user) {
    var chat = document.querySelector('#chat-window');
    loaderHTML = '<div class="loader-' + user + 
        '"><div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div></div>';
    chat.innerHTML += loaderHTML;
}

function removeChatLoader(user) {
    var loaders = document.querySelectorAll('.loader-' + user)
    loaders.forEach(loader => loader.remove());
}

function scrollDownChat() {
    var chat = document.querySelector('#chat-window');
    chat.scroll({ top: chat.scrollHeight, behavior: 'smooth' })
}

function checkVisible(elm) {
    var rect = elm.getBoundingClientRect();
    var viewHeight = Math.max(document.documentElement.clientHeight, window.innerHeight);
    return !(rect.bottom < 0 || rect.top - viewHeight >= 0);
}

function testSolution() {
    postMessage('/programming_assistant.check_task');
    displayMessage('Test my solution', 'user');
}

function giveHint() {
    postMessage('/programming_assistant.hint');
    displayMessage('Give me a hint', 'user');
}
