#!/usr/bin/env python3

from flask import Flask
from flask import render_template
from flask import request
from flask import jsonify
from cachelib import FileSystemCache
from difflib import SequenceMatcher
from datetime import datetime
import re
import requests
import json
import csv

''' This API is a middleware API between the chatbot and the FE

Endpoints: /, /chat, /ide_data, /compile, /chat

 1) This API serves static Frontend files (html, css, javascript)
 2) Handles requests from the javascript and redirects it to the chatbot
 3) Stores the conversation history in the conversation_log.csv file
 4) Handles requests to compile Java code and redirects it to JDoodle (online Java compiler)
 5) Additionally, we recently added Code analysis functionality: 
 and extended this API with /code_analysis endpoint (this can be removed)
 
'''

app = Flask(__name__)

# External APIs and services
chatbot_api = 'http://localhost:5005/webhooks/rest/webhook'
compiler_api = 'https://api.jdoodle.com/v1/execute'

# Credentials for Jdoodle API
jdoodle_client_id = '2a526780f789d5f37e96152cd25f257f'
jdoodle_client_secret = '280f00453c2e7c3dddf084e27c9648072d78f4c1984af488148f32827f9eefcb'

# Dictionary that stores IDE data for each user ID
# Entries are being cleared every 30 minutes using cache timeout
ide_data_cache = FileSystemCache(cache_dir='ide_data_cache')

# Experimental: CA functionality in the middleware (frontend API)
with open("java_errors.json") as file:
	kb = json.loads(file.read())

# Path to the file where conversations are stored
conversation_log = 'conversation_log.csv'

# Renders index.html file
@app.route('/')
def home():
    return render_template('index.html')

# Endpoint to receive and redirect messages to chat service (chatbot)
@app.route('/chat', methods=['GET', 'POST'])
def send_chat_message():
    data = request.get_json()
    print(data)
    user_id = data['sender']
    message = data['message']
    try:
        res = requests.post(chatbot_api, json=data)

        # Store messages to conversation log
        with open(conversation_log, 'a', newline='') as file:
            writer = csv.writer(file)
            writer.writerow([user_id, message, datetime.now()])
            writer.writerow(['chatbot', res.json()[0]['text'], datetime.now()])
        
        # Send the chatbot response back to FE
        return jsonify(res.json()), 200

    except requests.ConnectionError:
        return 'Chatbot is not responding', 500

# Endpoint send ide_data with a given user ID
@app.route('/ide_data', methods=['GET', 'PUT'])
def send_and_cache_ide_data():
    user_id = request.get_json()['user_id']

    if request.method == 'GET':
        data = {
            'user_id': 'User {} was not found'.format(user_id),
            'stdin': '',
            'script': '',
        }

        if ide_data_cache.get(user_id):
            user_ide_data = ide_data_cache.get(user_id)
            data['user_id'] = user_id
            data['stdin'] = user_ide_data[0]
            data['output'] = user_ide_data[1]
            data['script'] = user_ide_data[2]

        return jsonify(data), 200

    if request.method == 'PUT':
        # Store IDE data that was posted from FE in ide_data_cache dictionary
        stdin = request.get_json()['stdin']
        stdout = request.get_json()['stdout']
        script = request.get_json()['script']

        # use a simple cache to store ide data for 30 min
        ide_data_cache.set(user_id, [stdin, stdout, script], 60 * 30)

        return '', 204

# Endpoint to compile Java code using JDoodle API
@app.route('/compile', methods=['POST'])
def compile_code():
    script = request.get_json()['script']
    stdin = request.get_json()['stdin']
    user_id = request.get_json()['user_id']

    request_body = {
        'clientId' : jdoodle_client_id,
        'clientSecret': jdoodle_client_secret,
        'stdin': stdin,
        'script': script,
        'language': 'java'
    }
    request_body = json.dumps(request_body)
    try:
        res = requests.post(compiler_api, json=request_body)
        print(res)
        return jsonify(res.json()), 200
    except requests.ConnectionError:
        return 'Compiler is not responding', 500

    return jsonify(res.json())

# Endpoint to check how many credits are spent in JDoodle
@app.route('/credit_spent', methods=['POST'])
def check_credit():
    jdoodle_url = 'https://api.jdoodle.com/v1/credit-spent'
    request_body = { 
        'clientId' : jdoodle_client_id,
        'clientSecret': jdoodle_client_secret,
    }
    request_body = json.dumps(request_body)
    res = requests.post(jdoodle_url, json=request_body)
    return jsonify(res.json()), 200


# This receives compiler messages and returns the description of that message in natural language
# This was first a part of the chatbot but later moved to the middleware (frontend API)
@app.route('/code_analysis', methods=['POST'])
def analyse_compiler_message():
    error_text = request.get_json()['stdout']

    data = {'message': '',}

    if 'error: ' not in error_text and 'Exception' not in error_text:
        data['message'] = "Looks like you don't have any syntax or runtime errors in your code!"
        if error_text and error_text != 'Press run to execute':
            stdout = f'\nThe standart output of your Java program is:\n "{error_text}"'
            data['message'] = data['message'] + stdout
        return jsonify(data), 200

    keywords = [entry['name'].lower() for entry in kb['errors']]
    keyword_index = -1

    for line in error_text.splitlines():
        keyword_found = False
        for index, keyword in enumerate(keywords):
            if keyword in line.lower():
                keyword_found = True
                keyword_index = index
        if keyword_found:
            break
    
    if keyword_index != -1:
        error_type = kb['errors'][keyword_index]['error_type'].replace('_', ' ')
        message = f'This is a {error_type}.'
        description = kb['errors'][keyword_index]["description"]
        line_number = re.search("(?<=\.java:)[0-9]+", error_text)
        if line_number is not None:
            line_number = int(line_number.group(0))
            line_hint = f'Take a look at line {line_number}.'
            data['message'] = f'{message}\n{description}\n{line_hint}'
        else:
            data['message'] = f'{message}\n{description}'
        return jsonify(data), 200

    data['message'] = 'I don\'t have information about this error.'
    return jsonify(data), 200

# Runs Flask app
if __name__ == '__main__':
    app.run(port= 5000, debug=True)
