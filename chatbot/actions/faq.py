import requests
from rasa_sdk import Action, Tracker
from rasa_sdk.events import SlotSet, EventType, AllSlotsReset, FollowupAction
from typing import Text, List, Optional

# imports for fallback action
from rasa_sdk.executor import CollectingDispatcher
import json
from typing import Union, Any, Dict
from rasa_sdk.events import (
    UserUtteranceReverted,
    ConversationPaused,
)
import pandas as pd


INTENT_DESCRIPTION_MAPPING_PATH = "actions/intent_description_mapping.csv"

class ActionGreetBack(Action):
    """Utters a greeting template back to the user

    Checks if the user greets for the first time and utters messages accordingly.
    The slot already_said_hi is being set after the first greeting.
    """

    def name(self) -> Text:
        return "action_greet_back"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        already_greeted = tracker.get_slot("already_said_hi")

        if already_greeted:
            dispatcher.utter_message(template="utter_greet_again")
            return [FollowupAction(name="action_my_functionality")]
        else:
            dispatcher.utter_message(template="utter_greet")
            dispatcher.utter_message(template="utter_my_functionality")
        
        return [SlotSet("already_said_hi", "True")]


class ActionWhatCanYouDo(Action):
    """Utters about the bots functionality

    There are two cases: 
    1. User is currently solving the task -> limited functionality should be explained
    2. User is not solving the task -> full functionality should be explained
    """

    def name(self) -> Text:
        return "action_my_functionality"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        is_task_being_solved = tracker.get_slot("pa_task_is_being_solved")

        if is_task_being_solved:
            topic = tracker.get_slot("pa_topic")
            difficulty = tracker.get_slot("pa_difficulty")
            if topic and difficulty:
                if difficulty == 'easy':
                    difficulty = 'an easy'
                else:
                    difficulty = f'a {difficulty}'
                message = f'You are currently solving {difficulty} level {topic} exercise.'
                dispatcher.utter_message(template='utter_my_limited_functionality')
                dispatcher.utter_message(message)
            else:
                dispatcher.utter_message(template='utter_my_limited_functionality')
                dispatcher.utter_message(message)
        else:
            dispatcher.utter_message(template="utter_my_full_functionality")
  
        return []

class ActionRestart(Action):
    """Resets the conversation

    Removes all Slot values and starts from initial greeting again.
    """

    def name(self) -> Text:
        return "action_restart"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        return [AllSlotsReset()]

class ActionDefaultAskAffirmation(Action):
    """Asks for an affirmation of the intent if NLU threshold is not met."""

    def name(self) -> Text:
        return "action_default_ask_affirmation"

    def __init__(self) -> None:
        self.intent_mappings = pd.read_csv(INTENT_DESCRIPTION_MAPPING_PATH)
        self.intent_mappings.fillna("", inplace=True)
        self.intent_mappings.entities = self.intent_mappings.entities.map(
            lambda entities: {e.strip() for e in entities.split(",")}
        )

    def run(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[EventType]:

        intent_ranking = tracker.latest_message.get("intent_ranking", [])
        if len(intent_ranking) > 1:
            diff_intent_confidence = intent_ranking[0].get(
                "confidence"
            ) - intent_ranking[1].get("confidence")
            if diff_intent_confidence < 0.1:
                intent_ranking = intent_ranking[:2]
            else:
                intent_ranking = intent_ranking[:1]

        # for the intent name used to retrieve the button title, we either use
        # the name of the name of the "main" intent, or if it's an intent that triggers
        # the response selector, we use the full retrieval intent name so that we
        # can distinguish between the different sub intents
        first_intent_names = [
            intent.get("name", "")
            if intent.get("name", "") not in ["basic_faq", "advanced_faq", "chitchat", "confirmations",
                                              "greetings", "programming_assistant", "general"]
            else tracker.latest_message.get("response_selector")
            .get(intent.get("name", ""))
            .get("full_retrieval_intent")
            for intent in intent_ranking
        ]

        message_title = (
            "Sorry, I'm not sure I've understood " "you correctly 🤔 Do you mean..."
        )

        fallback_message = (
            "I might not have an answer for that... Could you rephrase your question?"
        )

        entities = tracker.latest_message.get("entities", [])
        entities = {e["entity"]: e["value"] for e in entities}

        entities_json = json.dumps(entities)

        buttons = []
        for intent in first_intent_names:
            button_title = self.get_button_title(intent, entities)
            if "/" in intent:
                # here we use the button title as the payload as well, because you
                # can't force a response selector sub intent, so we need NLU to parse
                # that correctly
                buttons.append({"title": button_title, "payload": button_title})
            else:
                buttons.append(
                    {"title": button_title, "payload": f"/{intent}{entities_json}"}
                )

        buttons.append({"title": "Something else", "payload": "/trigger_rephrase"})

        dispatcher.utter_message(text=message_title, buttons=buttons)

        return []

    def get_button_title(self, intent: Text, entities: Dict[Text, Text]) -> Text:
        default_utterance_query = self.intent_mappings.intent == intent
        utterance_query = (self.intent_mappings.entities == entities.keys()) & (
            default_utterance_query
        )

        utterances = self.intent_mappings[utterance_query].button.tolist()

        if len(utterances) > 0:
            button_title = utterances[0]
        else:
            utterances = self.intent_mappings[default_utterance_query].button.tolist()
            button_title = utterances[0] if len(utterances) > 0 else intent

        return button_title.format(**entities)

class ActionDefaultFallback(Action):
    def name(self) -> Text:
        return "action_default_fallback"

    def run(
        self,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any],
    ) -> List[EventType]:
    
        dispatcher.utter_message(template="utter_fallback")
        return [UserUtteranceReverted()]


# adapted from rasa-demo chatbot
def get_last_event_for(tracker, event_type: Text, skip: int = 0) -> Optional[EventType]:
    skipped = 0
    for e in reversed(tracker.events):
        if e.get("event") == event_type:
            skipped += 1
            if skipped > skip:
                return e
    return None


class ActionStackoverflow(Action):

    def name(self) -> Text:
        return "action_stackoverflow"

    def run(self, dispatcher, tracker, domain) -> List[EventType]:
        # if ActionStackoverflow isn't invoked directly via intent (but as a supplement after unsatisfactory bot answer)
        # we extract the user_question from previous event.
        if not tracker.latest_message['intent'].get('name') == 'stackoverflow':
            user_event_with_query = get_last_event_for(tracker, "user", skip=1)
            if user_event_with_query:
                user_question = user_event_with_query.get("text")
            else:
                dispatcher.utter_message(text="Sorry, I can't answer your question.")
                return []
        else:
            user_question = tracker.latest_message.get('text')

        res = requests.get(

            'https://api.stackexchange.com/2.2/search/advanced?'

            f'order=desc&sort=relevance&q={user_question}&tagged=Java&site=stackoverflow')

        res = res.json()

        if res.get('items'):
            link = res.get('items')[0]['link']
            title = res.get('items')[0]['title']

            dispatcher.utter_message("Here's what I found on StackOverflow:")
            dispatcher.utter_message(f'[{title}]({link})')
        else:
            dispatcher.utter_message("I did not find any matching questions on [StackOverflow](https://stackoverflow.com)")
            dispatcher.utter_message('I recommend you post your question there.')

        return []
