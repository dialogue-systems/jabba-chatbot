from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet

import os, json, re, requests, javalang, hashlib

url_base = "http://localhost:5000"
url_ide_data = url_base + "/ide_data"

jdoodle_url = "https://api.jdoodle.com/v1/execute"
jdoodle_client_id = "da3fe764f1093fb4dd6ffd3796b547cd"
jdoodle_client_secret = "c78cb0d32330d073ce99c7afa725f068fea81e5be3a6ce42921e76088f6eb8c3"

path_pa_tasks  = "actions/knowledgebase/pa_tasks.json"
path_pa_theory = "actions/knowledgebase/pa_theory.json"


def get_task_id(topic, difficulty):
    snake_case = f"{topic}_{difficulty}"
    camel_case = ''.join(x.capitalize() or '_' for x in snake_case.split('_'))
    return camel_case


class ActionSetTopic(Action):
    """Actions set topic and difficulty look at the previous intent 
    from the user and sets the slot based on that intent
    """

    def name(self) -> Text:
        return "action_pa_set_topic"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        
        topic = tracker.latest_message['intent'].get('name')
        if (topic == "programming_assistant.if_else"):
            return [SlotSet("pa_topic", "if_else")]
        elif (topic == "programming_assistant.loops"):
            return [SlotSet("pa_topic", "loops")]
        elif (topic == "programming_assistant.arrays"):
            return [SlotSet("pa_topic", "arrays")]
        elif (topic == "programming_assistant.data_structures"):
            return [SlotSet("pa_topic", "data_structures")]
        elif (topic == "programming_assistant.mixed_tasks"):
            return [SlotSet("pa_topic", "mixed_tasks"),  SlotSet("pa_category", "practice")]
        else:
            return [SlotSet("pa_topic", None)]


class ActionSetDifficulty(Action):
    """Actions set topic and difficulty look at the previous intent 
    from the user and sets the slot based on that intent
    """

    def name(self) -> Text:
        return "action_pa_set_difficulty"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        
        difficulty = tracker.latest_message['intent'].get('name')
        if (difficulty == "programming_assistant.get_task_easy"):
            return [SlotSet("pa_difficulty", "easy")]
        elif (difficulty == "programming_assistant.get_task_medium"):
            return [SlotSet("pa_difficulty", "medium")]
        elif (difficulty == "programming_assistant.get_task_hard"):
            return [SlotSet("pa_difficulty", "hard")]
       
        else:
            return [SlotSet("pa_difficulty", None)]


class ActionSetCategoryPractice(Action):
    """Set category are simply slot setters
    They set pa_category slot to practice or theory whenever the action is called
    """

    def name(self) -> Text:
        return "action_pa_set_category_practice"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        return [SlotSet("pa_category", "practice")]


class ActionSetCategoryTheory(Action):
    """Set category are simply slot setters
    They set pa_category slot to practice or theory whenever the action is called
    """

    def name(self) -> Text:
        return "action_pa_set_category_theory"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        return [SlotSet("pa_category", "theory")]


class ActionSetTaskIsBeingSolved(Action):
    """These actions set or remove the slot for PA use case
    This is used to distinguish the CA and PA paths
    """

    def name(self) -> Text:
        return "action_pa_set_task_is_being_solved"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        return [SlotSet("pa_task_is_being_solved", "True")]


class ActionRemoveTaskIsBeingSolved(Action):
    """These actions set or remove the slot for PA use case
    This is used to distinguish the CA and PA paths
    """

    def name(self) -> Text:
        return "action_pa_remove_task_is_being_solved_slot"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        return [SlotSet("pa_task_is_being_solved", None)]


class ActionUtterTopic(Action):
    """Utters response templates about the chosen topic or difficulty"""

    def name(self) -> Text:
        return "action_pa_utter_topic"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        
        topic = tracker.get_slot("pa_topic")

        if topic is None:
            print('Slot "topic" is not being set correctly')
            return []

        utterance = 'utter_pa_user_{}'.format(topic)
        dispatcher.utter_message(template=utterance)
        return []

class ActionUtterDifficulty(Action):
    """Utters response templates about the chosen topic or difficulty"""

    def name(self) -> Text:
        return "action_pa_utter_difficulty"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        
        difficulty = tracker.get_slot("pa_difficulty")

        if difficulty is None:
            print('Slot "difficulty" is not being set correctly')
            return []      

        utterance = 'utter_pa_user_chose_{}'.format(difficulty)
        dispatcher.utter_message(template=utterance)
        return []


class ActionGetTask(Action):
    """Finds and responds with a task template
    
    This action retrieves the task template for selected topic and difficulty 
    from the json knowledgebase. The template is in the form of Java code. 
    The template is added to the http response as a custom json attribute.
    """

    def name(self) -> Text:
        return "action_pa_get_task"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        topic = tracker.get_slot("pa_topic")
        if not topic:
            dispatcher.utter_button_template("utter_login_confirm")
            raise ValueError(f"pa_topic slot is not set (pa_topic = {topic}")

        difficulty = tracker.get_slot("pa_difficulty")
        if not difficulty:
            raise ValueError(f"pa_difficulty slot is not set (pa_difficulty = {difficulty}")

        task_id = get_task_id(topic, difficulty)

        if not os.path.exists(path_pa_tasks):
            raise FileNotFoundError(f"Failed to load pa tasks database at {os.path.abspath(path_pa_tasks)}")

        with open(path_pa_tasks) as file:
            tasks = json.load(file)

            if not task_id in tasks:
                raise KeyError(f"Failed to load {task_id} from {os.path.basename(path_pa_tasks)}")

            task = tasks[task_id]
            template = self.generate_template(task_id, task)

            if difficulty == 'easy':
                difficulty = 'an easy'
            else:
                difficulty = 'a ' + difficulty
            topic = topic.replace('_', ' ')

            dispatcher.utter_message(f"I prepared {difficulty} level task for {topic}. You can now find it in the IDE on the left.", json_message={"template" : template})

        return []

    def generate_template(self, task_id, task):
        from textwrap import wrap
        description_lines = wrap(task["description"])
        description = "\n".join(description_lines)

        example = task["testcases"][0]

        # TODO: prints don't work for arrays, because java sucks
        template_code = f""\
            f"/* ===== {task_id.upper()} =====\n"\
            f"{description}\n"\
            f"*/\n\n"\
            f"public class {task_id}\n"\
            f"{{\n"\
            f"    public static void main(String[] args)\n"\
            f"    {{\n"\
            f"        System.out.println(task({example['input']}));\n"\
            f"        // this should print: {example['output']}\n"\
            f"    }}\n\n"\
            f"    public static {task['signature']}\n"\
            f"    {{\n"\
            f"        // TODO\n"\
            f"    }}\n"\
            f"}}\n"

        return template_code


class ActionGetTheory(Action):
    """Retrieves theory from the json knowledgebase"""

    def name(self) -> Text:
        return "action_pa_get_theory"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        topic = tracker.get_slot("pa_topic")

        if not os.path.exists(path_pa_theory):
            raise FileNotFoundError(f"Failed to load pa theory database at {os.path.abspath(path_pa_theory)}")

        with open(path_pa_theory) as file:
            theoryResources = json.load(file)

            if not topic in theoryResources:
                raise KeyError(f"Failed to load {topic} from {os.path.basename(path_pa_theory)}")

            theoryResource = theoryResources[topic]

            source = theoryResource["source"]
            if type(source) == list:
                if len(source) > 1:
                    source = ", ".join(source[:-1]) + f" and {source[-1]}"

            dispatcher.utter_message(f"{theoryResource['description']}")
            dispatcher.utter_message(f"You can read more about {theoryResource['title']} at {source}")

        return []


class ActionCheckTask(Action):
    """Runs testcases after the user has finished solving the task template

    Generates a test class with the test inputs and outputs.
    Extends the Java task template with the test class.
    Runs the code using compiler API and compares test outputs 
    with actual outputs (using Objects.equals() in Java).
    Counts 'true' and 'false' and utters if a test case failed.
    """

    def name(self) -> Text:
        return f"action_pa_check_task"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        category = tracker.get_slot("pa_category")
        is_task_being_solved = tracker.get_slot("pa_task_is_being_solved")

        if is_task_being_solved != "True":
            dispatcher.utter_message(template='utter_pa_test_not_possible')
            return []

        if category != "practice":
            raise ValueError(f"Failed to check task: pa_category != \"practice\" (pa_category = {category})")

        topic = tracker.get_slot("pa_topic")
        if not topic:
            raise ValueError(f"pa_topic slot is not set (pa_topic = {topic}")

        difficulty = tracker.get_slot("pa_difficulty")
        if not difficulty:
            raise ValueError(f"pa_difficulty slot is not set (pa_difficulty = {difficulty}")

        task_id = get_task_id(topic, difficulty)

        if not os.path.exists(path_pa_tasks):
            raise FileNotFoundError(f"Failed to load pa tasks database at {os.path.abspath(path_pa_tasks)}")

        with open(path_pa_tasks) as file:
            tasks = json.load(file)

            if not task_id in tasks:
                raise KeyError(f"Failed to load {task_id} from {os.path.basename(path_pa_tasks)}")

            task = tasks[task_id]

            user_id = tracker.current_state()["sender_id"]
            ide_data = requests.get(url=url_ide_data, json={"user_id": user_id}).json()
            code = ide_data["script"]

            test_code = self.remove_public_from_taskclass(code, task_id, dispatcher)
            if not test_code:
                return []

            seperator = hashlib.sha512().hexdigest()
            script = "\n".join((self.get_additional_includes(), test_code, self.generate_testclass(task_id, task, seperator)))

            jdoodle_request = {
                "script": script,
                "clientId": jdoodle_client_id,
                "clientSecret": jdoodle_client_secret,
                "language": "java",
            }

            response = requests.post(jdoodle_url, json=jdoodle_request)
            output = response.json()["output"]

            if not seperator in output:
                dispatcher.utter_message("Your code didn't compile!")
                return []

            if output.count(seperator) != 2:
                dispatcher.utter_message("There seems to be a runtime error in the \"task\" function.")
                return []

            results = output.split(seperator)[1]

            if results.count("true") == len(task["testcases"]):
                dispatcher.utter_message("Yay, you did it! All tests passed!")
                dispatcher.utter_message(template="utter_pa_practice_end")
                return [SlotSet("pa_task_is_being_solved", None)]
            else:
                testcase_index = results.split("false")[0].count("true")
                testcase = task["testcases"][testcase_index]

                message = f"Hmmm, something about your solution seems to be wrong.\n"\
                          f"The testcase with input {testcase['input']} failed. (expected result: {testcase['output']})"
                dispatcher.utter_message(message)
                return []
        
        return []

    def remove_public_from_taskclass(self, code, task_id, dispatcher):
        try:
            tree = javalang.parse.parse(code)
        except (javalang.parser.JavaSyntaxError, javalang.parser.JavaParserError) as error:
            print(f"Failed to parse code: {error}")
            dispatcher.utter_message("There seems to be a syntax error in your code.")
            return None
        
        for tpe in tree.types:
            if tpe.name == task_id:
                if "public" in tpe.modifiers:
                    start = len("\n".join(code.split("\n")[:tpe.position.line - 1])) + tpe.position.column
                    public_pos = code.rfind("public", 0, start)
                    return code[:public_pos] + code[public_pos + len("public"):]

                else:
                    return code

        dispatcher.utter_message(f"Could not find class \"{task_id}\". Maybe you renamed it.")

    def generate_testclass(self, task_id, task, seperator):
        testcases_code = "\n".join(f"        System.out.print({task['comparator']}({testcase['output']}, {task_id}.task({testcase['input']})));" for testcase in task["testcases"]) + "\n"
        testclass_code = f""\
            f"public class Test_{seperator}\n"\
            f"{{\n"\
            f"    public static void main(String[] args)\n"\
            f"    {{\n"\
            f"        System.out.print(\"{seperator}\");\n"\
            f"{testcases_code}"\
            f"        System.out.print(\"{seperator}\");\n"\
            f"    }}\n"\
            f"}}\n"

        return testclass_code

    def get_additional_includes(self):
        additional_includes = ""\
            "import java.util.Arrays;\n"\
            "import java.util.Objects;\n"

        return additional_includes

class ActionGetHint(Action):
    def name(self) -> Text:
        return "action_pa_get_hint"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        category = tracker.get_slot("pa_category")
        is_task_being_solved = tracker.get_slot("pa_task_is_being_solved")

        if is_task_being_solved != "True":
            dispatcher.utter_message(template='utter_pa_hint_not_possible')
            return []

        if category != "practice":
            raise ValueError(f"Failed to get hint : pa_category != \"practice\" (pa_category = {category})")
    
        topic = tracker.get_slot("pa_topic")
        if not topic:
            raise ValueError(f"pa_topic slot is not set (pa_topic = {topic}")

        difficulty = tracker.get_slot("pa_difficulty")
        if not difficulty:
            raise ValueError(f"pa_difficulty slot is not set (pa_difficulty = {difficulty}")
        
        last_hint_index = tracker.get_slot("pa_last_hint_index")
        if last_hint_index == None:
            last_hint_index = "1"
        else:
            last_hint_index = str(int(last_hint_index) + 1)

        task_id = get_task_id(topic, difficulty)

        if not os.path.exists(path_pa_tasks):
            raise FileNotFoundError(f"Failed to load pa tasks database at {os.path.abspath(path_pa_tasks)}")

        with open(path_pa_tasks) as file:
            tasks = json.load(file)

            if not task_id in tasks:
                raise KeyError(f"Failed to load {task_id} from {os.path.basename(path_pa_tasks)}")

            task = tasks[task_id]
            if ("hint" + last_hint_index) not in task:
                last_hint_index = "1"

            dispatcher.utter_message(task['hint' + last_hint_index])
            return [SlotSet("pa_last_hint_index", last_hint_index)]

        return []

class ActionSwitchTask(Action):
    def name(self) -> Text:
        return "action_pa_switch_task"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        is_task_being_solved = tracker.get_slot("pa_task_is_being_solved")

        if is_task_being_solved == "True":
            message = "Looks like you haven't solved the current exercise yet. Do you really want to proceed to another one?"
            dispatcher.utter_message(message)
        else:
            dispatcher.utter_message(template='utter_pa_ask_for_topics')
            return [
                SlotSet("pa_task_is_being_solved", None),
                SlotSet("pa_category", 'practice'),
                SlotSet("pa_difficulty", None),
                SlotSet("pa_topic", None)
                ]

        return []
