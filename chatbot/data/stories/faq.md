## FAQ - initial path 
> check_initial_greeting
* general.i_have_a_question
    - utter_team_faq
    - utter_faq_examples

## Basic FAQ - Basic FAQ is single turn
* basic_faq
    - respond_basic_faq

## FAQ - Advanced FAQ is followed by stackoverflow - affirm
* advanced_faq
    - respond_advanced_faq
    - utter_ask_faq_help
* confirmations.affirm OR confirmations.maybe
    - utter_glad_to_help

## FAQ - Advanced FAQ is followed by stackoverflow - deny
* advanced_faq
    - respond_advanced_faq
    - utter_ask_faq_help
* confirmations.deny
    - action_stackoverflow

## FAQ - Advanced FAQ interruption - affirm
* advanced_faq
    - respond_advanced_faq
    - utter_ask_faq_help
* advanced_faq
    - respond_advanced_faq
    - utter_ask_faq_help
* confirmations.deny
    - action_stackoverflow

## FAQ - Advanced FAQ interruption - deny
* advanced_faq
    - respond_advanced_faq
    - utter_ask_faq_help
* advanced_faq
    - respond_advanced_faq
    - utter_ask_faq_help
* confirmations.affirm OR confirmations.maybe
    - utter_glad_to_help

## FAQ - Advanced FAQ interruption
* advanced_faq
    - respond_advanced_faq
    - utter_ask_faq_help
* basic_faq
    - respond_basic_faq

## FAQ - Stackoverflow intent handles out of scope questions
* stackoverflow
    - action_stackoverflow