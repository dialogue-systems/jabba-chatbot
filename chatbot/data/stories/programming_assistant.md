## PA - select topic - if else
> check_initial_greeting
* programming_assistant.assist
    - utter_greet_pa
    - utter_pa_ask_for_topics
* programming_assistant.if_else 
	- action_pa_set_topic
	- slot{"pa_topic": "if_else"}
	- action_pa_utter_topic
	- utter_pa_ask_for_type
> check_pa_topic_selected

## PA - select topic - arrays
> check_initial_greeting
* programming_assistant.assist
    - utter_greet_pa
    - utter_pa_ask_for_topics
* programming_assistant.arrays
	- action_pa_set_topic
	- slot{"pa_topic": "arrays"}
	- action_pa_utter_topic
	- utter_pa_ask_for_type
> check_pa_topic_selected

## PA - select topic - loops
> check_initial_greeting
* programming_assistant.assist
    - utter_greet_pa
    - utter_pa_ask_for_topics
* programming_assistant.loops
	- action_pa_set_topic
	- slot{"pa_topic": "loops"}
	- action_pa_utter_topic
	- utter_pa_ask_for_type
> check_pa_topic_selected

## PA - select topic - data structures
> check_initial_greeting
* programming_assistant.assist
    - utter_greet_pa
    - utter_pa_ask_for_topics
* programming_assistant.data_structures
	- action_pa_set_topic
	- slot{"pa_topic": "data_structures"}
	- action_pa_utter_topic
	- utter_pa_ask_for_type
> check_pa_topic_selected

## PA - select topic - mixed tasks
> check_initial_greeting
* programming_assistant.assist
    - utter_greet_pa
    - utter_pa_ask_for_topics
* programming_assistant.mixed_tasks
	- action_pa_set_topic
	- slot{"pa_topic": "mixed_tasks"}
	- action_pa_utter_topic
	- action_pa_set_category_practice
	- utter_pa_ask_for_difficulty
> check_pa_category_selected

## PA - select category - theory affirm
>check_pa_topic_selected
* programming_assistant.get_theory
	- action_pa_set_category_theory
	- slot{"pa_category": "theory"}
	- action_pa_get_theory
	- utter_ask_if_user_wants_task
* confirmations.affirm
	- action_pa_set_category_practice
	- utter_pa_ask_for_difficulty
> check_pa_category_selected

## PA - select category - theory affirm END STATE
>check_pa_topic_selected
* programming_assistant.get_theory
	- action_pa_set_category_theory
	- slot{"pa_category": "theory"}
	- action_pa_get_theory
	- utter_ask_if_user_wants_task
* confirmations.deny
	- utter_pa_theory_end

## PA - select category - practice
>check_pa_topic_selected
* programming_assistant.get_task
	- action_pa_set_category_practice
	- slot{"pa_category": "practice"}
	- utter_pa_ask_for_difficulty
> check_pa_category_selected

## PA - select difficulty - easy
> check_pa_category_selected
* programming_assistant.get_task_easy OR programming_assistant.get_task_medium OR programming_assistant.get_task_hard
	- action_pa_set_difficulty
	- slot{"pa_difficulty": "easy"}
	- action_pa_utter_difficulty
	- action_pa_get_task
	- action_pa_set_task_is_being_solved
	- slot{"pa_task_is_being_solved": "True"}
> check_task_is_being_solved

## PA - select difficulty - medium
> check_pa_category_selected
* programming_assistant.get_task_medium
	- action_pa_set_difficulty
	- slot{"pa_difficulty": "medium"}
	- action_pa_utter_difficulty
	- action_pa_get_task
	- action_pa_set_task_is_being_solved
	- slot{"pa_task_is_being_solved": "True"}
> check_task_is_being_solved

## PA - select difficulty - hard
> check_pa_category_selected
* programming_assistant.get_task_hard
	- action_pa_set_difficulty
	- slot{"pa_difficulty": "hard"}
	- action_pa_utter_difficulty
	- action_pa_get_task
	- action_pa_set_task_is_being_solved
	- slot{"pa_task_is_being_solved": "True"}
> check_task_is_being_solved

## PA - test solution
* programming_assistant.check_task
	- utter_pa_waiting_for_tests
    - action_pa_check_task

## PA - give a hint
* programming_assistant.hint
    - action_pa_get_hint

## PA - user wants another task - user is currently solving another task
* programming_assistant.solve_another_task
	- action_pa_switch_task
* confirmations.affirm OR confirmations.maybe
	- action_pa_remove_task_is_being_solved_slot
	- utter_pa_ask_for_topics
> check_another_task_start

## PA - user wants another task but decides to remain solving the previous one
* programming_assistant.solve_another_task
	- action_pa_switch_task
* confirmations.deny
	- utter_pa_keep_solving
> check_task_is_being_solved

## PA - user wants another task - user is not currently solving any tasks
* programming_assistant.solve_another_task
	- action_pa_switch_task
> check_another_task_start

## PA - user wants another task loops
> check_another_task_start
* programming_assistant.loops
	- action_pa_set_topic
	- slot{"pa_topic": "loops"}
	- action_pa_utter_topic
	- action_pa_set_category_practice
	- utter_pa_ask_for_difficulty
> check_pa_category_selected

## PA - user wants another task arrays
> check_another_task_start
* programming_assistant.arrays
	- action_pa_set_topic
	- slot{"pa_topic": "arrays"}
	- action_pa_utter_topic
	- action_pa_set_category_practice
	- utter_pa_ask_for_difficulty
> check_pa_category_selected

## PA - user wants another task if else
> check_another_task_start
* programming_assistant.if_else
	- action_pa_set_topic
	- slot{"pa_topic": "if_else"}
	- action_pa_utter_topic
	- action_pa_set_category_practice
	- utter_pa_ask_for_difficulty
> check_pa_category_selected

## PA - user wants another task data structures
> check_another_task_start
* programming_assistant.data_structures
	- action_pa_set_topic
	- slot{"pa_topic": "data_structures"}
	- action_pa_utter_topic
	- action_pa_set_category_practice
	- utter_pa_ask_for_difficulty
> check_pa_category_selected

## PA - user wants another task mixed tasks
> check_another_task_start
* programming_assistant.mixed_tasks
	- action_pa_set_topic
	- slot{"pa_topic": "mixed_tasks"}
	- action_pa_utter_topic
	- action_pa_set_category_practice
	- utter_pa_ask_for_difficulty
> check_pa_category_selected

## PA - Task is too difficult interruption
> check_task_is_being_solved
* programming_assistant.too_difficult
	- utter_pa_too_difficult

## PA - Task is too easy interruption
> check_task_is_being_solved
* programming_assistant.too_easy
	- utter_pa_too_easy

## PA - Cant solve the task interruption
> check_task_is_being_solved
* programming_assistant.cant_solve
	- utter_pa_cant_solve