## initial path
* greetings.greet
    - action_greet_back
    - slot{"already_said_hi": "True"}
> check_initial_greeting

## Chitchat
* chitchat
    - respond_chitchat

## Greet with name
* greetings.inform{"person_name": "A name"}
    - utter_greet_name

## Answer about functionality
* general.what_can_you_do OR general.i_am_lost
    - action_my_functionality

## Respond to negative feedback
* general.negative_feedback
    - utter_handle_negative_feedback

## Goodbye
* greetings.goodbye
    - utter_goodbye

## Restart - affirm
* general.restart
    - utter_ask_if_restart
* confirmations.affirm
    - action_restart
    - action_greet_back
    - slot{"already_said_hi": "True"}

## Restart - deny
* general.restart
    - utter_ask_if_restart
* confirmations.deny OR confirmations.maybe
    - action_my_functionality

## Random confirmation
* confirmations.affirm OR confirmations.deny OR confirmations.maybe
    - utter_random_confirmation

## Fallback story
* out_of_scope
    - respond_out_of_scope