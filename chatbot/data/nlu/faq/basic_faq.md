###### BASIC - ABOUT JAVA ######

## intent:basic_faq/what-is-java
- What is java?
- What is Java
- what is java
- What is Java?
- whattt is java?
- Java programming language
- Java language
- What is java programming
- What is java programming
- what is Java programming?
- what is java programming language?
- what is Java prog language?
- Tell me something about Java
- tell me about Java programming
- whats Java?
- what's java programming?
- whats Java
- what is Java
- what do you know about Java
- can you tell me about Java
- Tell me something about Java programming language
- is Java a programming language
- Is Java a programming Language?
- is java a programming language?
- what kind of language is Java?
- what kind of language is java

## intent:basic_faq/when-was-java-developed
- When was java developed
- when did java show up
- How old is Java
- How old is Java programming language
- When was java developed
- when did java show up
- When was Java language developed
- when was java developed?
- when did Java show up?
- when was Java developed?
- when did Java show up?
- when did Java occur?
- When was the first Java programm written?
- what year did java show up?
- what year was java developed
- when was Java created
- what year was Java created
- what year was Java created?
- When was Java language created?

## intent:basic_faq/how-is-java-different
- How is Java different from other programming languages?
- How is Java different from other programming languages
- How is Java different from c++
- how is Java different from c++?
- whats the difference between python and Java
- how is Java different from pascal
- how is Java different from php
- Is Java different than c++
- What makes Java different?
- What makes Java different from other languages?
- Is Java different from c
- What makes Java unique
- What makes Java stand out from other languages
- whats the difference between Java and C++
- why is Java different
- why is java different?

## intent:basic_faq/javascript
- what is javascript?
- can I use javascript in java?
- what has javascript with java to do?
- java javascript difference
- whats javaskript?
- is java same as javascript?
- difference between java and javascript
- How is java different from Javascript?
- Is javascript same as Java?

## intent:basic_faq/what-is-java-for
- what can I do with java?
- What is java doing?
- what is java doing
- What can be done using java?
- What is Java for?
- what can i do with java
- what can i do with Java?
- what is java used for?
- what was java implemented for?
- what can i do using java?
- can I write programs using java?
- can i write code using java?
- what is Java language for?
- how to speak java?
- how to speak in Java?

## intent:basic_faq/what-is-jdk
- What is jdk?
- WHAT IS JDK?
- what is jdk
- what is JDK
- What is JDK
- What is JDK?
- what is Java development kit?
- What is java development kit?
- what is java development kit
- what is java dev kit?
- what does jdk stand for
- I don't understand what jdk is
- i dont undersant what jdk is...
- i dont get what JDK is
- What does JDK stand for
- what is jdk used for
- What is JDK used for

## intent:basic_faq/what-is-jre
- What is jre?
- WHAT IS JRE?
- what is JRE
- what is JRE
- What is JRE
- What is JRE?
- what is Java runtime environment?
- What is java runtime environment?
- what is java runtime env
- what is java runtime env?
- what does jre stand for
- I don't understand what jre is
- i dont undersant what jre is...
- i dont get what JRE is
- What does JRE stand for
- what is jre used for
- What is JRE used for

## intent:basic_faq/what-is-jvm
- What is jvm?
- WHAT IS JVM?
- what is jvm
- what is JVM
- What is JVM
- What is JVM?
- what is Java virtual machine?
- What is java virtual machine?
- what is java virtual machine
- what is java virt machine?
- what does jvm stand for
- I don't understand what jvm is
- i dont undersant what jvm is...
- i dont get what JVM is
- What does JVM stand for
- Why Java is platform independent?
- why is java platform independent

## intent:basic_faq/jvm-jdk-jre
- whats the difference between jdk and jvm?
- What is the difference between JDK and JVM?
- difference between JDK and JVM
- difference between jdk and jvm
- what's difference between JDK and JRE?
- what's the difference between jdk and jvm
- what's the difference between jdk and jre
- what is different between jdk jre
- How is jdk different from jvm
- Are JDK and JVM the same?
- are jdk and jvm same things?
- Are jdk and jre same things?
- Does jdk and jvm mean the same?
- does jdk and jvm mean same things?
- jvm vs jdk
- JVM vs. JDK
- JVM vs JDK
- JVM vs JRE
- JVM vs jdk
- jvm and jdk 

## intent:basic_faq/compilation-process-in-java
- What is the compilation process in Java
- what is the compilation process in jAVA?
- How does a compilation process in Java look?
- What are the steps in the Java compilation process?
- How Java code is compiled?
- Compilation step by step
- compile step by step
- Java compilation and Execution process
- java compilation and execution process
- compilation process of a Java programm
- Compilation process
- How is Java programm being compiled?
- Do you have to compile java
- Do you have to compile Java?

## intent:basic_faq/what-is-an-algorithm
- what is an algorithm
- what does 'algorithm' mean?
- what does algo mean
- what is an algo
- whats algo?
- what's algorithm
- what's algorithm?
- what is an algorithm?
- what is algorithm 
- what does an algorithm do?
- what does algorithm do
- what do algorithms do?
- what do algorithms do
- what does an algorithm mean
- what is an algorithm
- what do you mean by an algorithm
- what is algorithm in programming
- what is algorithmus
- what's algorithmus
- what does the term 'algorithm' mean?
- i don't understand what is an algorithm
- I don't get what algorithm means
- i don't understand what do you mean by algorithm
- give me a definition of an algorithm
- what is an algorithm
- what is algorithm


###### BASIC - JAVA COMMENTS ######

## intent:basic_faq/comments
- how can I make comments inside the code?
- How to write comments in Java
- How to write comments?
- comments
- COMMENTS IN JAVA
- comments java
- Java comments
- multi-line comments
- Multiline comments
- multiline comments
- comment code
- syntax for java comments
- java comments
- ignore code
- how to comment code
- whats the syntax for commenting 
- how can i comment out code


###### BASIC - BASIC FUNCTIONS ######

## intent:basic_faq/print
- how to print a number?
- how does System.out.println(); work
- how to print string?
- how does output work?
- print information

## intent:basic_faq/use-input-arguments
- what does String[] args mean?
- how can I use commandline arguments?
- how to use commandline args
- how to use command line input
- commandline input args

## intent:basic_faq/import-library
- import functions
- import librarys
- import libraries
- import utils
- use external library
- extern functions usage
- how do you import libraries
- how can i use externel libraries
- how to import utils

## intent:basic_faq/goto
- is there goto in java?
- what is goto?
- goto
- jump into line
- whats goto
- does goto exist in java?


###### BASIC - VARIABLES ######

## intent:basic_faq/what-is-a-variable
- variable
- Variable
- what is a variable
- what is a variable
- What is a variable?
- whats variable?
- What is a variable?
- What's a variable?
- What's a variable?
- what is a variable in Java
- what does a variable mean
- what does a variable mean?
- what does a variable mean in Java
- what does var mean
- what is a variable
- what is variable in programming
- what is a variable in programming
- Does Java have variables
- i want to know about variables
- I want to know about variables in Java
- What is a variable

## intent:basic_faq/how-to-declare-a-variable
- declare a variable
- declaration of a variable
- variable declaration
- declare a variable in java
- Declare a variable
- How can i declare a variable?
- How to declare a variable?
- How to declare a variable
- how to declare a variable
- how to declare a variable in Java
- how to define a variable?
- how to declare a variable in Java
- I want to define a variable in Java
- I want to define a variable in Java
- how to define a variable?
- How can I define a variable?
- How can i define a variable?
- how should i define a variable in java?
- how to declare a var
- how to declare a variable?
- I want to declare a variable
- How can one declare a variable in Java

## intent:basic_faq/how-to-initialize-a-variable
- initialize a variable
- initialise a variable
- initialization of a variable
- how can I initialize a variable?
- How can i initialize a variable
- How can I initialize a variable?
- how to initialise a variable?
- how to init a variable
- how to initialize a variable
- How to initialize a variable
- How to initialize a variable?
- how to initialisze a variable ?
- initialize a variable?
- init a variable in Java
- init a variable
- how to initialise a variable in Java
- I want to define a variable in Java
- I want to define a variable in Java
- how to define a variable?
- How can I define a variable?
- How can i define a variable?
- how should i define a variable in java?
- how to declare a var
- how to declare a variable?
- I want to declare a variable
- How can one declare a variable in Java

## intent:basic_faq/global-vs-local-variables
- what are global variables?
- what are global variables
- what is a global variable
- what is a global variable?
- What's a global variable?
- What is a global variable
- what are global variables and what does that mean
- what does global mean?
- global variables
- whats a global variable in Java
- what variables are global?
- What variables are global?
- why doesn't java see my variable?
- variable not defined
- global variables
- globl variabls
- global vs local variables
- local variables
- what are local variables?
- What are local variables?
- what is a local variable?
- What is a local variable?
- What's a local variable
- What are global and local variables?
- what is a local variable?
- What's a local variable
- whats a local and global variable
- lcal and global variables
- local versus global variables 
- local variables in Java
- I want to know about global variables in Java
- I want to know about local variables
- what's a local variabel
- scope of variable
- Scope of a variable
- variable scope
- Variable scope
- scope of local variables
- Scope of global variables

## intent:basic_faq/final
- constant values
- what does final mean
- public final int
- final classes
- final variables
- what is a final?
- whats a final value
- what is a final variable
- whats a final variable?
- what does it mean when a variable is declared with the final keyword?
- what does it mean when a variable is declared as final
- what does final mean in java
- what does the final keyword do

## intent:basic_faq/static-variables
- What is a static attribute?
- What is a static variable
- What's a static variabel?
- what is a static variable?
- what is a static variable in Java?
- static variables
- Static variables
- static var
- static variable
- Static variable
- 'static' variable
- static variable
- How is static attribute used?
- Static attributes in Java
- Static attribute
- sTATIC VALUE
- Static value

## intent:basic_faq/what-is-null
- what does null mean?
- what is null?
- difference null and 0
- non-initiliased value
- value not set
- null value
- empty values
- what is the difference between null and 0?
- whats the diference between nul and 0?
- what is the null value
- NULL
- null 



###### BASIC - PRIMITIVE DATA TYPES ######

## intent:basic_faq/data-types
- What data types does Java have?
- what datatypes does java have?
- What data types does Java have?
- what data types does java have
- what data types are included in Java
- what is a data type in Java?
- what is a data type
- What is a Data Type in Java?
- data types in Java
- Data types in Java
- Java datatypes
- Java Data Types
- data types
- Data Types
- data types
- Data types
- Datatypes
- Primitive datatypes
- What are primitive datatypes
- what are primitive data types in Java
- What datatypes does java include
- what data types are included
- what kind of data types
- what is a datatype
- what are the data types
- non primitive data types
- non-primitive data types in java
- Non-primitive datatypes

## intent:basic_faq/byte-data-type
- is byte a datatype?
- is byte a primitive data type
- what is a byte?
- What is a byte
- what is a 'byte'?
- How much is a byte
- How long is a byte?
- byte
- byte data type
- Byte in Java
- Bytes in Java
- What's the size of a byte
- whats the size of a byte?
- What's the size of a byte?
- what's the size of a byte in java
- Are bytes used in Java
- tell me about bytes
- tell me about byte data type
- what is a byte

## intent:basic_faq/short-data-type
- short in Java
- shorts in Java
- short dt
- short data type
- is short a primitive datatype?
- How big is short?
- What's the size of a short?
- whats the size of short?
- short datatype
- does java use shorts?
- does Java have short
- is short included in java
- what kind of data type is a short
- 'short'
- I want to know about shorts in java
- I want to know about short data type
- Short in java

## intent:basic_faq/int-data-type
- Int data type
- int in Java
- int in java
- what is an int?
- What's an int
- what is an int?
- what is int
- what is 'int'?
- what does 'int' stand for?
- what does 'int' mean?
- what does 'int' mean?
- what does int mean in Java
- what is an int
- what's the size of an int?
- whats the size of int?
- What's the size of int
- What's the size of int data type
- how big is an int
- how big is int?
- Is int a primitive data type
- is int primitive?
- int datatype
- int 
- int datatype in Java
- what the hell is a int?
- initialize int
- how to initialize an int
- hot to use int
- i want to know about int

## intent:basic_faq/long-data-type
- long data type
- longs in java
- long in java
- what is long?
- what does 'long' stand for?
- what does 'long stand for
- what does long mean?
- what is long
- what's the size of long
- What's the size of long?
- is long a datatype
- is 'long' used in Java
- initialize long
- initialize a long in Java
- Long in Java
- tell me about long datatype in java
- What is a long
- what is a long data type in java
- how to use long
- how to use longs
- long
- Long

## intent:basic_faq/float-data-type
- what is float in Java
- what is float in Java?
- What is float in java
- What is Float
- waht is a float in java
- float data type
- Float datatype
- float Data type
- float dt
- Floating point numbers
- float point numbers
- Floating point numbers
- floating point numbers
- floats in Java
- i want to know about floats
- how much is a float
- How much is a float?
- How big is float?
- What's the size of a float
- using floats
- using floating point numbers
- is float a primitive datatype
- is float primitive
- how to use floats
- how to use float
- float
- Float

## intent:basic_faq/double-data-type
- double
- Double datatype
- what is a double in Java
- What is a double in Java?
- What is a double?
- what is double
- Whats a double
- How to use double
- is double a datatype
- double data type in Java
- doubles in Java
- double datatype in Java
- double dt
- Doubles
- decimals in Java
- how to use decimals?
- can I use double for decimal numbers?
- can i use double to cound decimals
- How big is double
- what's the size of double?
- What's the size of a double
- is double a primitive data type

## intent:basic_faq/boolean-data-type
- bool
- Bool in Java
- boolaen in java
- does Java use booleans?
- are booleans used?
- How are booleans used?
- how are booleans used?
- what's the value of a boolean?
- i want to know about boolean data type
- is bool used in Java
- is boolean a primitive data type?
- Is bool primitive
- is bool a data type
- is boolean a datatype
- boolean in java
- what is boolean
- what is a boolean
- what is a boolean?
- what is a Bool?
- what is a boolean
- what is a Bool
- how to use booleans in Java
- How to assign a value to a boolean variable
- boolean data type
- Booleans in java
- What is a boolean in Java
- How to use true false values in java
- I want to use True and False values
- tRUE AND FALSE
- true and false in Java
- True and False values
- do booleans exist in Java?

## intent:basic_faq/char-data-type
- char data type
- chars in Java
- char in java
- what is a char?
- char compared to string
- what are characters?
- what is a chartcer
- char vs string
- single letter
- charectar
- what is a char
- is char a primitive data type
- is char primitive?
- how long in char?
- what is a char
- what is a 'char'
- character in Java

## intent:basic_faq/difference-int-long
- what is the difference between int and long?
- what's the difference between integer and long?
- whats the difference between int and long
- integer long difference
- long int difference
- whats the diference between int and long?
- How are int and long different?
- Whats the difference between int and long data types?
- are int and long the same
- are long and int the same

## intent:basic_faq/difference-double-float
- what is the difference between double and float?
- what's the difference between float and double?
- How are float and double different
- how are float double different
- should i use float or double?
- Should i use double or float?
- double float difference
- float double difference
- differences between float and double




###### BASIC - TYPE CASTING ######

## intent:basic_faq/what-is-casting
- what is casting
- What is casting in Java
- what does casting mean
- what is casting in Java
- does java support casting
- how to cast in java
- casting in java
- what is casting in Java?

## intent:basic_faq/cast-double-int
- how to cast a double to an int
- make int from double
- cast double to int
- double casting to integer
- double to int
- integer casting
- cast double


###### BASIC - ACCESS MODIFIERS ######

## intent:basic_faq/access-modifiers
- what are access modifiers
- What are access modifiers in java?
- How is visibility controlled in Java
- how is visibility controled
- visibility
- accessibility
- access modifier
- Java access modifier
- Access modifier
- public private difference
- public private protected
- what does protected mean
- difference package-protected protected
- difference public private
- how to use visibility
- hide function
- package private
- visability
- visebility
- visibilaty
- what is the difference between public and private?
- waht is the difference between private and protected?
- what's the diff between public and private
- how can i hide a function
- what is visibility in java
- how does visibility work in java?
- visibilaty in java programming

## intent:basic_faq/private-modifier
- what does private mean?
- What does private mean
- what is private
- what is the meaning of private
- What does private stand for?
- What does private mean?
- what's private?
- what does 'private' mean?
- What is private in Java
- what does private in Java mean?
- private keyword
- What does private keyword mean?
- i don't understand what private means
- I do not understand private

## intent:basic_faq/public-modifier
- what does public mean?
- What does public stand for?
- i don't understand what public means
- public keyword
- What does 'public' mean?
- what is public in Java
- what is public modifier
- What does public do?
- what does public mean?

## intent:basic_faq/protected-modifier
- what does protected mean
- What does protected mean?
- protected
- 'protected'
- What is protected modifier
- Why is protected used?

# basic_faq/package-protected-modifier
- 'package-protected'
- package-protected modifier
- what does package-protected do? 
- package-protected

## intent:basic_faq/static-keyword
- what does static mean? 
- why is main static?
- what is static in java?
- what is 'static'?
- What is static used for?
- static
- i don't know what static means
- What does static mean in Java?
- What is static used for?
- How can I use static 
- static keyword
- Static modifier
- static modifier
- static modify
- static word


###### BASIC - CONDITIONALS ######

## intent:basic_faq/conditional-statements
- How can I check conditions in Java
- how can i check conditions in Java?
- How to use conditions in Java?
- What are java conditional statements
- how is conditional logic implemented in java?
- How should i write conditions
- How should i use conditions
- conditional statements
- conditions in Java
- I want to implement a condition
- what conditional statements does java have
- What conditions does Java have
- How to write java conditions
- What conditional statements does java have?
- what other conditional statements does Java include?
- conditional logic in java
- conditional statements
- conditional
- conditions in Java
- How to implement logic in Java
- how to implement logic in java?
- logic in java
- how to implement logic in java
- how to make a decision in Java
- decision making statements in Java
- decision making in java

## intent:basic_faq/how-to-write-if-else
- How to write if than statement
- How to write if-else statement
- How to write if statement
- How to write if-else statement
- how to use if-else
- how to write if than else 
- if else syntax
- if-else syntax
- What is the syntax of ifelse?
- What is the syntax of if else statement?
- if-else statement
- How to write if-else statement
- if-else
- how to implement if-else 
- How to use if else

## intent:basic_faq/switch-case
- What does switch mean?
- What is "switch" in java?
- what is switch case in Java?
- what is switch-case?
- Explain me switch in java
- What is the syntax of switch
- syntax of switch
- Syntax of switch statement Java
- How does switch condition look like?
- How to write switch condition
- what is switch case?
- switch
- switch case
- many if-conditions
- if alternative
- swtich case

## intent:basic_faq/ternary-operator
- what is the ternary-operator in java?
- ternary-operator
- operator with three operands
- ternery oparator

## intent:basic_faq/de-morgans-law
- what is de morgan?
- What is de morgans law?
- waht is demorgans property
- what is de morgan 
- what logic does de morgan 
- De Morgan’s laws
- De morgan's property
- De Morgan's rule 
- De Morgan for Java operators

## basic_faq/flag-variables
- What is a flag variable
- what is flag variable in java
- conditional variable
- how to use flags in java
- Java flags
- what is a flag variable
- What is a Flag variabel?
- what is a flag variable

## intent:basic_faq/combine-booleans
- how can I check two conditions?
- how can I check two booleans?
- how can I check two bools?
- how can I check to bools?
- combine booleans
- multiple booleans if conditions
- combine bools



###### BASIC - CONTROL FLOW ######

## intent:basic_faq/control-flow-statements
- Control flow in Java
- how to implement control flow
- How to use control flow statements
- Control flow statements
- Branches and flows in java
- I want to know about control flow
- how to write control flow statements
- what statements are controling the flow
- what statements are controling the flow of a programm
- controlling the flow of a programm
- control flow branches
- Control flow 
- controlflow
- control flow statements

## intent:basic_faq/continue
- continue loop
- what does continue mean?
- skip loop iteration
- contnuie loop
- when can i use continue
- how can i skip a loop iteration?
- how do i skip a loop iteration
- how can i continue a loop

## intent:basic_faq/break-loop
- how do I stop a loop?
- break loop
- return inside loop
- stop a loop
- for loop stop
- stop while loop
- while loop stopp



###### BASIC - JAVA OPERATORS ######

## intent:basic_faq/java-operators
- What is an operator
- what is an operator?
- What is an operator?
- whats operator in java?
- What's an operator in Java?
- What kind of operators does java have?
- What kind of operators does java include
- Java operators
- java operators
- Operators in Java
- operators in java
- operations in java
- operators in java
- java symbols
- Symbols in java

## intent:basic_faq/arithmetic-operators
- How to add two numbers
- How to add two numbers?
- how to add numbers
- how to do arithmetic
- how to use arithmetical operators?
- How to perform arithmetical operations
- how to subtract two numbers
- how to multiply
- how to devide numbers
- what arithmetic operators does java have
- What arithmetic operators does java include
- Arithmetic operators in Java
- Arithmetic in java
- How to do arithmetics
- Arithmetic operators in Java
- how many arithmetic operators does java have?
- What arithmetic operators does java have>
- arithmetical operators in Java
- arithmetical operator

## intent:basic_faq/assignment-operators
- How to asign a value
- How to assign a value to an integer?
- how to assign a value
- assignment operators in java
- How to assign
- assign a value
- assignment operator
- assignment operator in java
- assign and add
- assign and subtract

## intent:basic_faq/comparison-operators
- How to compare two values in java
- How to compare two values?
- How to compare two values?
- comparison operators in java
- What are comparison operators?
- what are comparison operators?
- what kind of comparison operators exist
- java comparison operators
- Comparison operators in java
- java comparison operators
- What are comparison operators in Java?

## intent:basic_faq/logical-operators
- logical operators in java
- What are logical operators in java
- What logical operators are in Java?
- How to write greather than
- how to write less than
- how to use logical operators
- logical operators in Java
- show me a list of logical operators in java
- logical operators
- logical operations
- logical operator
- How many logical operators are there in java?
- how many logical operators does java have?
- what logical operatos does java have
- What logical operators are in java
- What logical operators are there in java?
- What logical operators
- how to write not x
- how to write not x in java
- logical operators 
- logical operators in java
- I want to know about logical operators in java
- I want to know about logical operators in Java
- negation in java
- how to negate an expression

## intent:basic_faq/bitwise-operators
- Bitwise operators in jAVA
- BITWISE operators in java
- bit wise operators
- integer AND
- int OR
- int XOR
- bit wise operations
- operation bitwise int
- bitwise operations
- which bitwise operators exist in java
- what is the bitwise operator for AND
- what is the bitwise operator for OR
- what is the bitwise operator for XOR
- bitwise operators for AND, OR and XOR
- how many bitwise operators does java have?

## intent:basic_faq/int-bitshift
- how can I shift bits in numbers?
- multiply int by 2
- divide int by 2
- bitshift
- bitshift operators
- multiply integer by 2
- divide integer by 2
- divide integer by two
- how to do bitshift in java
- what are the bit shift operators in java
- what are the bitshift operators in java

## intent:basic_faq/modulo
- how does modulo work?
- how to calculate the remainder
- waht does modulo mean?
- how to take the reminder
- How to calculate a reminder?
- What operator should i use to calculate a reminder?
- calculate rest of division
- remainder of division
- procent sign operator
- percent sign operator
- prozent sign

## intent:basic_faq/difference-compare-declare
- difference = and ==
- difference between compare and declare
- why doesn't if(a=b) work?
- if(a=b)
- numbers of equal signs
- equal sign
- equality sign
- whats the difference between = and ==?
- whats the difference between "=" and "=="
- check equality in if statement
- equality in if statement

## intent:basic_faq/compare-integer
- how to compare two integer?
- are two numbers the same?
- is there a way to compare two integer?
- two similar numbers
- are two numbers equal?
- how to compar two integers
- test if two ints are equal
- are two integers equal
- is there a way to compare two ints?

## intent:basic_faq/equals
- what does equals mean?
- What does equals mean?
- What does equals() mean?
- what does .equals means?
- how does equals work?
- equals for strings
- string.equals() function
- equals method for strings



###### BASIC - JAVA ARRAYS ######

## intent:basic_faq/what-is-array
- What is an array
- What is an array?
- What is an Array?
- what is array in Java
- What is an array in Java
- What is array in Java?
- ARRAY
- array
- what is an array in Java
- is array a data type
- Is array a data type?
- data type array
- is array a priitivie data type
- What is an array
- I don't understand arrays in Java
- I don't understand arrays
- explain me arrays
- Explain me arrays
- I need to know what array is
- what are arrays
- box of elements
- a box of elements
- array in java
- Array?
- what does array mean?
- is array an object
- is array a class
- is array an object in java
- what is an array object
- what is array class

## intent:basic_faq/declare-array
- How to use arrays
- how do I declare an array?
- How to create an Array?
- how to write an Array?
- how to create array
- how do arrays work?
- How to create an Array?
- what does [] mean?
- what are these square brackets?
- what does int[] mean?
- declaration arrays
- more than one integer at once
- flere integer
- multiple integers in one variable
- How to use arrays in Java
- how to write an array
- How are arrays implemented
- How to use arrays
- How to use Arrays?
- how to write arrays in jAVA
- How to use Arrays>
- How to write an array
- How to write array
- use array
- create an array

## intent:basic_faq/array-size
- How to get the size of an array?
- how to get the length of an array?
- How to get the length of a lis in java?
- how to get the length of a vector
- how to get the length of an array in java
- check the length of an array
- find out a size of an array
- how to find the size of an array?
- how to check how long is my array
- how do I check the size of an array?
- how many elements are there in the array
- array size
- array length
- check number of array elements

## intent:basic_faq/resize-an-array
- How to resize an array in java?
- How can I resize an array?
- how can i resize an array
- How to change the size of an array
- How to add an extra element to the array?
- How to change the size of array?
- How to resize array?
- Is it possible to resize the array in java?
- Can I resize an array in Java
- Resize array

## intent:basic_faq/copy-an-array
- how can I copy an array?
- Can i make a copy of array
- Can I make a copy of an array?
- how to make a duplicate array
- How to copy array elements?
- Move array elements to another array
- copy values of array into new array
- duplicate array
- create new array with values of an old array
- copy numbers of array
- copy array
- copy one array to another array
- copy one array to another one

## intent:basic_faq/min-max-array
- get minimum variable of an array
- get maximum variable of an array
- how to get min/max of an array
- how to get the max element in an array
- How to find a maximum element in array
- How to find a minimum element in array
- maximum value inside an array
- minimum value inside an array
- smallest int in array
- biggest integer array
- how can i get the biggest int in an array

## intent:basic_faq/iterate-over-array
- How to iterate over an array in java
- how to iterate over array
- how to iterat over aray
- how do you iterate over an array
- how do you iterate over arrays
- how to print all array elements
- use for loop with array
- get all array elements
- how do you get all elements of an array
- iterate array elements
- iterate over array elements

## intent:basic_faq/access-array-element
- how to get array at position 7
- How to get array element?
- How to get array element?
- How to get an element from an array?
- how to access array element
- How to access an element in array?
- acces array at position 4
- how do you access a specific array element
- get array at position
- how to access an array element
- array get single element

## intent:basic_faq/last-element
- get last element of an array
- last element array
- array last element
- access last element of array
- get final element of an array
- how can i get the last element of an array

## intent:basic_faq/array-indexes
- What is an index in array
- how to index an array
- what is the first index of an array
- do arrays start from 1 in Java
- do arrays start from 0 in Java?
- First element of an array
- how to get the first element from an array
- Array indexing
- index of an array
- Get the first element in array
- get the first element of the array
- how are arrays indexed?
- What indexes do arrays use?
- From what number do array indexes start?

## intent:basic_faq/get-index-of-array-element
- How to get an index of an element in array?
- get index by value in Array
- get index of an array element
- How to get the index of an array element?
- How to know the position of an element inside array?
- Find the index of an array element in Java.
- How to find the index of an array element?
- How to get the position of an array element?
- Get index when you know the value
- How to find position of array element?
- get position of array element
- Find index of an element in given array
- I have an array i have an element and i need to find the position
- find position by value
- array indexOf
- Array indexof()
- array indexOf()

## intent:basic_faq/reverse-an-array
- reverse order of array
- reverse array
- change order in array
- array order reversion
- how to reverse an array
- how can i revers order of an array

## intent:basic_faq/add-array-element
- How to add a new element to an array
- How to add a new element to an array?
- I want to add a new element to array
- how to add element to Java array?
- How to add element to array
- How to add an element to the array
- how to add to an array
- insert a new value to array
- add new array element
- Append an element to an array
- How to append an element to java array
- how to append an element
- Append to arraylist
- add element to arraylist
- Add a new element to arraylist
- How to add element to arraylist?
- Add array element
- add array
- Append array

## intent:basic_faq/remove-array-element
- How to remove an element from array
- How to remove element from Java array?
- How to remove an element from array?
- how to delete an element from array
- I want to delete an element from array
- how to remove an element fro array
- how to remove array element

## intent:basic_faq/concatinate-arrays
- how to put two arrays together?
- how to concat two arrays?
- How to concat two arrays?
- merge two arrays
- adding to arrays together
- melt arrays together
- concatenate array
- concatinate arrays
- how can i concatenate two arrays
- how to merge arrays
- merging arrays into one

## intent:basic_faq/arraylist
- what is arraylist?
- what is arraylist
- What is arrayList?
- What is arraylIST?
- I have heard abour arraylist what is it?
- tell me abour arraylist
- How can i use arraylist?
- How to use ArrayList?
- Is ArrayList a data structure?
- How is arraylist used?
- Array list
- Lists in java
- Lists in Java
- Java list
- Do java have Lists?
- Does Java have lists?
- what is an arrayList?
- how to construct an arrayList?
- use ArrayList
- arraylist usage
- arraylist
- linkedlist

## intent:basic_faq/difference-ArrayList-Array
- array vs arraylist
- Array vs. ArrayList
- how is ArrayList different?
- whats the difference between array and arraylist?
- Why should I use Arraylist?
- Should i use ArrayList instead?
- what is the difference between array and arraylist
- different arraylists and arrays
- why using arraylist
- why should I use arrayList instead of arrays?
- why use arraylist
- difference arraylists and arrays
- difference arrays and arraylist

## intent:basic_faq/2D-array
- how to make a matrix in java
- How to use twodim arrays
- Hw to write 2d array?
- How to use 2d arrays in Java?
- 2d arrays in java
- Multidimentional list in java
- Multidimentional array in java
- multidim array
- Nested arrays
- how to create a two-d array
- create two dim array
- create matrix with array
- matrix in java
- 2D array
- two dimensional arrays
- nested arrays



###### JAVA STRINGS ######

## intent:basic_faq/what-is-a-string
- What is a string in java
- what is a string in java
- what is a string in Java
- What is a string?
- what is a String?
- What is a string?
- String in Java
- I dont understand what string is
- What is a string in Java?
- What are strings in java?
- is string a data type
- is string a data type?
- is string a data type?
- Is STRING a primitive data type?
- What is a string in Java
- what is a String
- string
- Strings
- Java string
- Java strings
- Is string an object?
- is string a class?
- what is a string?
- String data type
- String class
- class String

## intent:basic_faq/create-a-string
- How to use strings
- How to create a String?
- How to use strings in Java?
- How to create a string in Java?
- how to generate a string?
- How to define a string?
- how to get a new string object
- how to create a new string
- how can I define a new string
- make new string
- define string
- generate string object
- How to represent a word in java
- sequence of letters
- Sequence of letters
- words in java
- Words in java
- sentences in java
- how to store a sentence
- how to store a word

## intent:basic_faq/string-length
- string length
- How to know how long is my word?
- How to know the leght of my string?
- How to find length of a string?
- Length of a string
- size of a string
- Size of a string
- how do I check the size of a string?
- how many chars are there in the string?
- how many letters are there in the string
- amount of characters in string
- string size
- string length
- length of string
- stirng lengh

## intent:basic_faq/toString-method
- what is a toString method?
- how to print objects?
- can I print objects?
- toString method
- what is toString
- toStringmethod
- how can i print objects in java
- what does the toString method do?

## intent:basic_faq/get-letter-in-string-by-index
- how to access one letter in a string?
- get char of string
- how to access one char in a string
- get part of a string
- get letter of string
- get leter of strig
- get letter by position in string
- how can I access only one character in a string

## intent:basic_faq/concatinate-strings
- how to put two strings together?
- concatinate strings
- concatinate two strings
- how do you concatenate two strings together
- add two strings
- expand string
- append string
- append strings

## intent:basic_faq/split-string
- is it possible to split a string
- string splitting
- divide a string into halves
- divide a string
- cut string in half
- cutting strings into halves
- how can i split a string at whitespace
- how can i split string at specific char
- how can i split a string at a character
- split string at char
- splitting string at charakter
- split string at a character

## intent:basic_faq/new-line
- how to make a new line in a string?
- go into next line string
- linebreak strings
- string line breaks
- new line
- newline
- how to get a new line in a string
- write linebreak in strinf

## intent:basic_faq/compare-strings
- How can I compare two strings?
- comparation two strings
- are two strings equal?
- java doesn't compare strings correctly
- are two strings the same
- two identical strings?
- comparison of two strings
- how to compare two string
- compare two strings
- test if two strings are the same



###### BASIC - LOOPS ######

## intent:basic_faq/what-is-a-loop
- What is a loop?
- whats a loop?
- What is a loop?
- what's loop?
- what does loop mean
- How to loop
- how to iterate
- How to use loops
- what does a loop mean
- what is a loop
- What is a loop in Java?
- what is a loop?
- what is aloop
- whats aloop
- I want to iterate
- What kind of loops does java have?
- What loops des Java have?
- what loops are there in Java?
- are there 2 types of loops in java?
- How many types of loops are in Java?
- what are loops
- Loops in Java
- loops
- Loop
- Loops

## intent:basic_faq/for-loop
- What is a for-loop
- What is a for loop?
- What's a 'for' loop?
- How to use a for-loop
- how to declare a for-loop?
- How to write a for loop?
- how to write a "for" loop?
- How to use a for-loop?
- Whats the syntax for loop
- For loop syntax
- for loop syntax
- Syntax of a for loop?
- how to use for
- how do for-loops work?
- how does a for-loop work
- for-loop
- for loop usage
- what are for loops
- what are for-loops
- Waht are for looops
- what are foor loops
- How do you declare a for loop?
- How do you declare a for-loop
- for schleife
- FOR LOOP
- forloop
- For loop
- "for" loop
- for loop
- for-loop

## intent:basic_faq/for-each-loop
- is there a for-each loop in java?
- is there a for-each loop?
- Does Java have for each loop?
- How to use for-each loop?
- how to use for-each loop
- I want to do something for each element
- iterate over each element
- for each loop java
- for in loop
- how do i write a for each loop in java
- write for-each loop
- syntax for for-each loop
- "for each" loop
- For-each loop
- for each loop
- for each

## intent:basic_faq/while-loop
- "while" loop
- How to write a while loop?
- How to use a while loop?
- What's the syntax of the while loop?
- What's the syntax of the while loop?
- While loop in Java
- "while" loop
- how to declare a while-loop?
- how to declare a while loop
- how to deklare a whileloop?
- how to use while
- how do while-loops work?
- how do while loops work
- how does a while-loop work
- while loop
- how do while-loops function
- while-loop usage
- how do you declare a while-loop
- how to create a while-loop?
- how to write a while loop
- how to use a while loop
- while loop
- "while" loop
- while schleife
- while

## intent:basic_faq/difference-do-while
- What is a do while loop?
- How does a do ... while loop look?
- How to write a do-while loop?
- How to create a do-while loop?
- what is the difference between while and do-while?
- What is the difference between while and do ... while?
- Whats the difference between while and do while?
- What's the difference between "while" and "do while"?
- what is a do-while-loop
- whats the difference between do-while and while
- what is the difference of do while and while
- difference while do-while
- differences while, do while
- do-while compared to while
- while compared to do-while
- while do-while
- do-while while
- "do .. while" loop

## intent:basic_faq/if-loop
- what is an if-loop?
- if loop
- if-loop
- are there if-loops?
- are there if loops
- do if-loops exist
- whats a if loop
- are there if loops in java

## intent:basic_faq/infinite-loop
- how to make an infinite loops
- make a while loop for ever
- how to write an infinite while-loop
- while(true)
- never stopping loop
- never stopping while loop
- infinite loop



###### BASIC - JAVA METHODS ######

## intent:basic_faq/what-is-a-method
- WHat is a method?
- whats a method
- what's a method?
- what does method mean
- what is a method
- methods in Java
- Java methods
- what is a method?
- how can I outsource code
- methods
- methods
- Java methods
- Method in java
- Is function the same as a method?

## intent:basic_faq/what-is-a-function
- What is a function
- What is a function
- functions
- function
- Do functions exist in Java?
- example of a function
- how does one write a function in java?
- How to create a function?
- Is function the same as a method?

## intent:basic_faq/main-method
- What is a main method?
- what is a main method in Java?
- what does main mean?
- what is main method used for?
- what is main method?
- how to write a main method
- syntax of main method
- How does main method look like?
- Main function
- main method

## intent:basic_faq/use-a-method
- How to create a method?
- how to write a method?
- how to use a method?
- Syntax of the method
- write a method
- How to write a method in Java
- How to use methods in Java
- how to declare a method in java
- How to declare a method?
- How to create a method?
- syntax of methods
- How to create a function
- How to write a function
- How to create a function?
- How to use functions in Java?
- how to use a method
- How to execute a method in java?
- How to execute a method
- usage methods
- usage of methods
- how to use a method?
- how do you use methods?
- function calling
- how to call a function?
- how do I call a function?
- how to call a function?

## intent:basic_faq/call-a-method
- how to call a method?
- How to call a method
- How to call a method?
- How to call a method in Java?
- Coll a method in jAVA
- call method
- call a method
- Call a method

## intent:basic_faq/method-parameters
- Pass information to methods
- How to pass information to methods in Java
- Method parameters in java
- Method parameters
- method parameters
- method attributes
- How to use method parameters
- method parameters?

## intent:basic_faq/multiple-parameters
- can i use ultiple parameters inside a method?
- Can I use more than one parameter?
- Multiple parameters
- More than one method parameter
- multiple method parameters
- more method parameters

## intent:basic_faq/void
- what does void mean?
- void method
- return void
- return nothing in method
- void methods
- what are void methods
- how can i return nothing in a method
- My method does not return anything
- I want to write a function that does not return anything
- How to create a method that does not return anything
- I want to write a silent function

## intent:basic_faq/static-methods
- what is a static method?
- What is a static method in Java
- What is a static method in java?
- whats a static method
- what is a static method
- What is a static method?
- what is a static method?
- static methods
- static method
- Static method
- static function
- static function

## intent:basic_faq/pass-by-reference
- what does pass-by-reference mean?
- pass-by-reference
- using references in java
- are there pointers in java?
- pointer in java
- reference-passing
- pass refrence
- refarence passng
- how do you use references in java?
- how to use references in java
- how can I implement pointers in java?
- how to pass references in java?
- how do i pass references
- how are parameters passed by reference
- what does pass-by-reference mean?
- what does pass by reference mean
- what does passing by reference mean
- how is a parameter passed by reference?

## intent:basic_faq/pass-by-value
- how do I pass values?
- how to copy values of numbers?
- copy object value
- duplicate variable
- copy value of variable
- copy variable value
- passing values
- pass value
- how do I pass-by-values?
- pass-by-value
- how can i pass a parameter by value
- how is a parameter passed by value?
- how can i duplicate a variable
- How can i copy a value of variable?
- pass by value

## intent:basic_faq/static-methods
- Static methods in Java
- What is a static method in Java?
- how to create a static method in java?
- static method
- static methods
- static methods in java
- global functions in java
- global method in java
- Static method

## intent:basic_faq/static-public-difference
- Whats the difference between static and public?
- static public difference
- How is static different
- Why should i use static?
- static vs public
- static or public
- static versus public





###### BASIC - OOP ######

## intent:basic_faq/what-is-oop
- What is oop?
- what is oop
- What is OOP?
- wHAT is oop?
- What is OOP
- whats oop
- What is object oriented programming?
- What does oop stand for?
- what does "oop" stand for
- What do letters oop mean
- what is object oriented
- Why is Java object oriented?
- Is Java object oriented?
- OOP

## intent:basic_faq/oop-concepts
- What are the OOPs concepts?
- What are the OOP concepts?
- what are the key concepts of OOP?
- What are the key concepts of OOP?
- what are the main aspects of oop
- concepts of oop
- Tell me more about OOP
- Concepts of object oriented programming
- OOP concepts
- OOP definition
- oop description

## intent:basic_faq/what-is-an-object
- Whats the difference between a class and an object?
- What is an object in Java?
- What is an object?
- what is an object
- What is an object
- Show me an example of an object in Java
- Show my an example of an object
- Object in Java
- what is an object?
- objects in java
- java objects
- difference class objects
- Example of an object
- example of an object in Java

## intent:basic_faq/what-is-a-class
- What is a class?
- what is a class
- what is a class in java?
- what is a class?
- How to use classes?
- how to write a class
- how to use classes
- What is a class in Java?
- Tell me about classes
- tell me more about classes
- What do you know about classes?
- I want to know what a class is?
- What is a class?
- what's a class?
- whats class?
- what is a class?

## intent:basic_faq/constructor
- how to use constructor of class?
- how to use a constructor?
- what is a constructor
- what is a constructor?
- construct a class
- preinitialise a class 
- deconstructor
- init class object
- constructor
- constcrutor
- how do you use a constructor of a class
- whats a constructor of a class?
- what is a constructor
- what is a constrcutor
- what does constructor mean
- how can i use a constructor of a class?
- how can i initialise a class object
- Show me an example of a constructor
- constructor example

## intent:basic_faq/class-attributes
- What is a class attribute in Java?
- Class attribute
- What are class attributes?
- class attribute
- what is an attribute?
- What is an attribute?
- what is attribute?
- class attributes
- class values
- What is a class attribute
- example of a class attribute
- fields

## intent:basic_faq/access-class-attributes
- How to access class attributes?
- How to modify class attributes?
- How to change class values?
- How to get class values?
- How to access class values?
- how to access class values
- how to achieve class values
- how to set class values
- How to set class values

## intent:basic_faq/class-methods
- What is a class method in Java?
- Class methods in Java
- What are class methods
- Class methods in Java
- method within a class
- create a class method
- How to use a class method?
- What are class methods in Java?

## intent:basic_faq/enum
- what are enumerations?
- enums
- java enumerations
- enumerations
- enum
- enumretions
- whats a enum in java
- what are enumerations in java
- what are enums
- what are java enumerations