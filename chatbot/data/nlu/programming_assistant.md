## intent:programming_assistant.assist
- Let's code together
- lets code together
- can we code together
- could we code together
- maybe we can code together
- maybe you can assist me
- I would like to learn Java
- I would like to learn java
- I would like to learn Java programming
- I would like to learn Java programing
- I would like to learn java programming
- I want to learn Java
- I want to learn
- I want to elarn Java
- I want to code together
- I want you to assist me
- I want to learn java programming
- I want to learn java programing
- I want to learn programming
- I want to learn programing
- I want to learn Java
- I want a tutor!
- java tutor
- Java tutor
- JAVA tutor
- please assist me
- please asist me
- can you assist me
- can you asist me
- could you assist me
- could you asist me
- lets learn java programming
- lets learn java programing
- lets learn java
- lets learn Java
- let's learn java
- programming assistant
- programing assistant
- I want to solve a task
- I want to solve an exercise
- I want an excersise
- give me an exercise
- Give me exercise
- Give me an exercise
- give me exercise
- Give me an exercise
- Please give me an exercise
- I want an exercise
- exercise pls
- Give me a task
- I want to solve a task
- Give me a task to solve
- give me a task to solve
- i want to solve a task
- I want to solve a task
- I want an excersise
- I want to solve a task
- I want to solve something

## intent:programming_assistant.get_task_easy
- Easy
- easy
- Start slow
- I want it easy.
- I want an easy problem
- Give me an easy problem
- I am a beginner

## intent:programming_assistant.get_task_medium
- Medium
- medium
- Not so hard
- Make it medium

## intent:programming_assistant.get_task_hard
- Hard
- hard
- Difficult
- I want a hard problem
- I want a difficult problem

## intent:programming_assistant.if_else
- if
- if else
- if-else
- I want to learn if statements
- I want to learn if and else statements
- Give me some if else
- Let's practice if else.

## intent:programming_assistant.loops
- Loops
- I want to learn loops
- Give me some loops
- Let's practice loops.

## intent:programming_assistant.arrays
- Arrays
- I want to learn arrays
- Give me some arrays
- Let's practice arrays.

## intent:programming_assistant.data_structures
- Data structures
- I want to learn data structures
- Give me some data structures
- Let's practice data structures

## intent:programming_assistant.mixed_tasks
- mixed tasks
- I want to solve mixed tasks
- Give me mixed tasks
- Let's practice mixed tasks

## intent:programming_assistant.get_task
- practice
- task
- give me a task
- i want a task
- train me
- training
- give me a coding challenge

## intent:programming_assistant.get_theory
- theory
- Theory
- theoretical resource
- give me theory
- read about

## intent:programming_assistant.hint
- can you give me a hint
- i would like a hint
- i need a hint
- show me a hint
- i'd like a hint
- i want a hint
- give me another tip
- show me another one
- give me another one
- please give me a hint
- get me a hint
- give me another clue
- give me another suggestion
- show me a clue
- yes i want a hint
- yes give me another one
- could you give me another hint
- yes i'd like a hint
- i need another hint
- thanks, i want another one
- give me a hint

## intent:programming_assistant.check_task
- test my solution please
- Test my solution
- test my solutions
- i solved my exercise, test it
- test my solved exercise
- Test it please
- Test my solution
- Test My solution
- Could you test it
- I am finished. test it
- Test my Solution
- test it please
- test it
- Test
- check
- test my solution
- test solution
- check solution
- check my task
- check task
- test task
- test my program
- check my program
- test my code
- check my code
- test code
- check code
- Did i solve my tast correct?
- Is my solution right?
- is my solution correct?
- did i do it right
- did i do it right?
- is this correct?
- is my solution right?
- did i do everything right?
- did i do everything correctly?

## intent:programming_assistant.solve_another_task
- I want to solve a different task
- I want to solve another task
- I want to solve another task
- i want to solve another task
- give me another exercise
- Give me another exercise
- give me another exercise please
- another exercise
- I want another exercise
- give me other exercise
- a different exercise
- I want to solve a new task
- i want to solve another task
- I want another task
- I want a different task
- I want new task
- I want a different practice
- i want a different exercise
- i want another task
- give me another task
- I want a new exercise
- I want a different exercise
- give me a different task
- give me new exercise
- please give me a new task
- please provide me a new exercise
- Give me another one
- give me another task
- Give me one more
- Give me one more task
- Give another task
- Another task
- Can I solve more tasks?
- I want to solve a different task
- What other tasks do you have?
- can i have a task from another category?
- choose a different category
- different category Task
- can i get a new task?
- can you give me another task?
- i want a task from a different category
- i don't like this category
- i want a different task
- give me a different task
- can i get a new task please
- can i get some other task?
- i don't like this task
- i want to solve another task
- I want to solve another Task
- can i get a different task?
- can i get another Task
- can i get another task?
- can i get a task about something Else
- can i get a task about something else?
- different task please
- this task is shit

## intent:programming_assistant.too_difficult
- this task is too dificult
- this task is too hard
- it's impossible to solve this task
- this task is to difficult
- this task is not easy at all
- this task is too difficult
- this task is very difficult
- this tas is very hard
- this task is way to hard
- exercise is too difficult
- this exam is too difficult
- this exam is too hard
- this is too hard
- this is too difficult
- this task is too difficult

## intent:programming_assistant.too_easy
- this task is too easy
- this task is too easy
- it's impossible to solve this easy
- this task is to easy
- this task is not easy at all
- this task is too easy
- this task is very easy
- this tas is very hard
- this task is way to hard
- exercise is too easy
- this exam is too easy
- this exam is too easy
- this is too easy
- this is too easy
- this task is too easy
- it was way too easy
- too simple for me
- this task is too simple for me
- too simple
- this exercise is too easy

## intent:programming_assistant.cant_solve
- I can't solve it
- i cant solve it
- This task is unsolvable
- I can't solve this task
- I cant solve this task
- i can't solve this task
- no way I can solve this
- can't solve
- cant solve
- i can't find a solution
- i don't find solution
- I can't find the solution
- really cant solve this task
- I can't solve this one either
- cant solve it either
- cant solve it neither
- I can't solve this exercise
- I can't solve this exercise
- this is too hard
- this is too tough
- This is too tough for me
- tough cookie
- this is tough

## intent:programming_assistant.more
## - More 
## - What exercises do you have?
## - Show me all the exercises
## - Show me all you got
## - Is this all ?
## - Show list of topics