#!/bin/sh
cd chatbot
rasa run --enable-api --cors "*" &
rasa run actions &
cd ../frontend
python3 api.py