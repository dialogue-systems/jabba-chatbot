# Conversational Agent for Java Programming

Jabba is a conversational agent that teaches the basics of Java programming language. This repository provides code and documentation from the seminar work *Dialoge mit Computer - Chatbots 2020*. This readme describes current functionality of the project, discusses its limitations and possible directions for the future work.

1. [Overview of the files](#1-overview-of-the-files)
2. [Run the project locally](#2-run-the-project-locally)
3. [Jabba's Functionality](#3-jabba's-functionality)
    - [Programming Assistant](#programming-assistant)
    - [Answering Java related questions](#answering-java-related-questions)
4. [Components](#4-components)
    - [Frontend](#frontend)
    - [Middleware API](#middleware-api)
    - [External APIs](#external-apis)
    - [Chatbot](#chatbot)
5. [Future Work](#5-future-work)
6. [Study resources - *Where to start?*](#6-study-resources)

Jabba is also accessible at [http://jaba-chatbot.informatik.hu-berlin.de](http://jaba-chatbot.informatik.hu-berlin.de). To access the website you need to use [Informatik-VPN](https://www.informatik.hu-berlin.de/de/org/rechnerbetriebsgruppe/dienste/openvpn-server) with your HU account.


This project was carried out by a group of seminar students. Supervisor: Dr. Nguyen-Thinh Le (nguyen-thinh.le@hu-berlin.de), student assistant: Lukas Garbas (lukasgarba@gmail.com). We are happy for your interest to contribute and work on this project further 🎉.


## 1. Overview of the files

The main two directories of this project are `chatbot` and `frontend`. Chatbot was developed using [Rasa Open Source](https://rasa.com/) library and we also use [Flask](https://flask.palletsprojects.com/en/1.1.x/) API to serve static html, css and js files.

### Chatbot

`chatbot/data/` - folder for all your training data. This folder consist of training data for natural language understanding - NLU, and the training data for the conversation flow (also known as a Dialogue Manager) - stories.

`chatbot/data/stories/` - contains training data for Dialogue Management (previously called Rasa Core) component.

`chatbot/data/nlu/` - contains training data for NLU component. Since the appearance of ResponseSelector, this folder can also contain some chatbot responses for a single-turn dialogue use case.

`chatbot/models/` - stores trained machine learning models. After training the model, .tar file with model weights will be stored. In general, Rasa will store two models: one for NLU (DIETClassifier) and one for Dialogue Manager (TEDPolicy).

`chatbot/actions/` - python modules for custom action code. Actions are python code modules, where you can run some custom code, such as connect to external APIs or even simply create response templates for the user.

`chatbot/config.yml` - configuration file for NLU and DM algorithms. Rasa provides various NLP algorithms that be used for Chatbot development. The framework itself is structured in two components: NLU and Dialogue Manager (policies). This file is the configuration what algorithm pipelines should be used for NLU and for DM models.

`chatbot/domain.yml` - list of all intents, entities (from training data), predefined responses and actions. Domain is the universe in which your chatbot operates. All intents and responses are placed there. 

`chatbot/endpoints.yml` - configures endpoint for the action server.

### Frontend

`frontend/api.py` - Flask API that serves static files and sends requests to the chatbot. The complete functionality is explained [here](#middleware-api).

`frontend/static/css` - css files for the index site.

`frontend/static/js` - js code for chat-window and the IDE.

`frontend/templates/index.html` - html index file.

`frontend/ide_data_cache` - to provide the IDE content to the chatbot, we temporary store it in the cache. Consider finding a better solution that doesn't use the cache in future versions.

`frontend/conversation_log.csv` - we did not use a database and simply store conversation logs in a csv file. This can also be done in the chatbot itself (rasa actions) or by configuring a database with rasa.

### Deployment setup

The current version of Jabba runs on localhost and the dependencies that were added when deploying the project can be ignored. To deploy the project we used [Docker](https://docs.docker.com/get-started/) containers together with [docker-compose](https://docs.docker.com/compose/) and we chose [nginx](https://docs.nginx.com/nginx/) as the webserver. The latest version can be found in jabba-v1.

`nginx/` - webserver configuration

`docker files` - docker setup.

`docker-compose.yml` - docker-compose setup to configure docker network with 4 services

The docker setup is already prepared but in order to run docker-compose you will need to replace the *localhost* to service names (frontend_api, rasa, rasa_action) in following files: `frontend/api.py`, `chatbot/programming_assistant.py`, `chatbot/endpoints.yml`.

Please contact Dr. Nguyen-Thinh Le (nguyen-thinh.le@hu-berlin.de) if you want to get the access to the Virtual Machine [http://jaba-chatbot.informatik.hu-berlin.de](http://jaba-chatbot.informatik.hu-berlin.de) (ladmin@41.20.38.76) where Jabba can be deployed.


## 2. Run the project locally

#### Dependencies:
* Python 3.7 
* Rasa 1.9 (Rasa Open Source library. We used a version of rasa from 2019)
* Flask (used to create a middleware api and serve static html, js and css files)

#### 1. Install dependencies

Create and activate a virtual environment to install dependencies (recommended)

```bash
python3 -m venv env
source env/bin/activate
```

```bash
pip install -r requirements.txt 
```

This project uses 3 services: rasa (port 5005), rasa actions (port 5055) and frontend (port 5000)

#### 2. Train the model

```bash
cd chatbot
rasa train
```

It takes around 10-15 minutes to train the model on the current training data. The model will be stored in `models/` directory.

#### 2. Run chatbot service (on locahost)

```bash
rasa run --enable-api --cors "*"
```

Chatbot server runs on port 5005

#### 3. Run action server

```bash
rasa run actions
```

When debugging, we recommend to use the following flags: 

```bash
rasa run actions --actions actions --auto-reload -vv
```

(*-vv* option for debugging output)

(*--auto-reload* to enable auto-reloading of modules containing Action subclasses. (handy for development. changes directly applied to current dialogue session))


#### 4. Run web-frontend (on localhost)

```bash
cd web-frontend
python3 api.py
```

Frontend, together with middleware API runs on port 5000 and is accessible via http://127.0.0.1:5000



## 3. Jabba's Functionality

We decided to focus on two main use-cases for Jabba: programming assistant and answering Java related questions.

### Programming Assistant

One use-case of Jabba is to teach Java programming. Initial ideas that are partially implemented in the chatbot are: __provide Java exercises for the user__, __give theory resources about Java concepts__, __examine (test) if the solution of the exercise is correct__, __give hints that can help to solve exercises__. To implement these goals, one needs both: a chatbot, and an IDE to write Java code. 

Messages *'I want to learn Java'* or *'give me a task to solve'*  triggers the path of programming assistant. To create the complete conversation, we train TEDPolicy model with stories (training data for Dialogue Manager). We decided to use buttons to make sure that user follows the path of programming assistant and does not ask unrelated questions.

The current path of the programming assistant looks as follows: 1. user asks the chatbot to teach Java (f.e *'I want to learn Java'*, *'give me a task to solve'* or simply presses the button to solve an exercise) 2. Jabba asks to select a concept 3. Jabba asks if a user wants to read theory or proceed to exercise 4. the exercise is sent from rasa actions to the frontend and displayed in the IDE. Now user can ask for hints or to test the solution (one can use buttons in the IDE for that). During the project, we prioritized the use-initiative (user controls the dialogue) strategy. We tried to make stories short and create a chatbot that doesn't feel like a strictly controlled rule-based bot. However, even by setting quite limited goals for this use-case, the chatbot still needs some improvement in the conversation flow and stories.

To understand algorithms and our code for Programming Assistant, we highly recommend to look at the following resources: [DIETClassifier](https://www.youtube.com/watch?v=vWStcJDuOUk&t=83s), [TEDPolicy](https://www.youtube.com/watch?v=j90NvurJI4I), understand how chat buttons work and look at the code in the communication between middleware API and actions.

Please feel free to experiment with Programming Assistant and extend or even rewrite it with your ideas.


### Answering Java-related questions

A few examples of questions that Jabba should answer: *'What are the concepts of OOP?'*, *'What is a loop?'*, *'How to find a max value in an array?'*.

Another important functionality that we decided to work on is *answering questions about Java programming*. This goal is tough to achieve since users can ask anything and we might need tons of intents to properly handle all questions. Most importantly, the chatbot should answer questions that are related to the exercises in Programming Assistant.

We used [ResponseSelector](https://rasa.com/docs/rasa/chitchat-faqs/) and looked at how different projects are using chatbots for FAQs with many intents. In response selector, user questions are single-turn question-answer pairs that can interrupt anywhere in the conversation. To avoid having more than 100 question-answer pairs in one response selector, we decided to split questions into two parts: *basic FAQ* and *advanced FAQ*. We attempt to categorize questions into basic and advanced concepts of Java. Basic FAQ has 117 question-answer pairs, where advanced FAQ has 50.

We also make use of [StackExchange API](https://api.stackexchange.com/) and create an intent for questions that should be answered by providing a link to stack overflow discussion. 

The only algorithm that is responsible for answering Java-related questions is [ResponseSelector](https://rasa.com/docs/rasa/chitchat-faqs/). Here is a [whiteboard explanation](https://www.youtube.com/watch?v=2jvyWngHEJM) and a [blog post](https://blog.rasa.com/response-retrieval-models/) about it. 

Feel free to improve the functionality of Java question answering. Alternatively, you can search for other ways to do Question Answering in chatbots. It can also be something completely unrelated to Rasa.

### Additional functionality

Chitchat is using ResponseSelector and is implemented in the same way as *answering Java related questions*. It can interrupt anywhere in the conversation and should not disturb the general conversation flow.

## 4. Components

### Web-frontend

- Basic web UI for testing purposes.
- Written using plain js, css, html.
- Components: chat window, text editor - IDE.

We did not have a dedicated team to focus on frontend or middleware. When working on the project, we quickly realized that these components are very important and you can not work on Programming Assistant without having an IDE to write Java code and solve exercises. So far, the frontend is implemented using plain html, css and js files and does not use any frontend framework (f.e. [React](https://reactjs.org/)). There is a lot of room for improvement to make a robust frontend that would work on all browsers and would be easily extendable.

### Middleware API

[Flask](https://flask.palletsprojects.com/en/1.1.x/) application that has the following functionality:

- Serve static files for the website (index html file, css and js)
- Receive chat messages requests from the client (js code) and send requests to chatbot service
- Use external Jdoodle API to compile Java code that was sent from the client
- Cache the content of the IDE and send it to the chatbot service if requested
- Store the conversation log in a csv file.

### External APIs

__Jdoodle__: This project makes use of [Jdoodle API](https://www.jdoodle.com/compiler-api/) to compile and run Java code. The connection to API is in two places: frontend/api.py (to compile and run java code and display the result for the client) and chatbot/actions/programming_assistant.py (to execute test cases by running the code and comparing the output).

__StackExchange__: As discussed in [Answering Java related questions](##-Answering-Java-related-questions), we search for answers to some questions using [StackExchange API](https://api.stackexchange.com/).

All external APIs come with limitations (limited number of requests) and require authorization tokens. Tokens might expire and you will need to get new ones.

### Chatbot

The chatbot uses three models: [DIETClassifier](https://www.youtube.com/watch?v=vWStcJDuOUk&t=83s) (for NLU), [TEDPolicy](https://www.youtube.com/watch?v=j90NvurJI4I) (for Dialogue Manager) and [ResponseSelector](https://rasa.com/docs/rasa/chitchat-faqs/) (for Chitchat and Java related questions). Both use-cases of the chatbot use actions `actions/faq.py` and `actions/programming_assistant`.

__Intents__: chatbot has 34 intents that are being classified by DIETClassifier. Chitchat has 37 question-answer pairs that are trained using ResponseSelector. Basic FAQ has 117 and advanced FAQ has 50 question-answer pairs.

__Knowledgebase__: `actions/knowledgebase` includes csv files with descriptions for Java exercises and links for theory resources.

__Fallback__: we use two stage fallback together with buttons with intent descriptions from `actions/intent_description_mapping.csv`.




## 5. Future Work
 
__Well needed changes__: By the end of 2020 Rasa released a new version Rasa 2.x. We highly recommend to read [migration guide](https://rasa.com/docs/rasa/migration-guide#rasa-110-to-rasa-20) and bring needed files into the new format. Its also very important to double check stories since there are places where the chatbot does not respond.

__Should I add more training data?__: Sure, you can also create new intents.

__How can I test the chatbot?__: You will need to create a test set to evaluate or compare the performance of NLU component. Testing Dialogue Manager can also be done similarly but it is still a pretty new [feature](https://rasa.com/docs/rasa/testing-your-assistant/). 

__How to improve Programming Assistant?__: We think that existing exercises are too difficult for beginners. To add, one could make use of code templates or exercises where the user is asked to fill in the gaps. There are many directions to explore in Programming Assistant and a lot of other ideas can be adapted from the literature on Pedagogical Agents.

__How to improve Question Answering of Java related questions?__: Please consider adding more training data for existing intents in basic and advanced FAQs. You can also try completely different approaches of Question Answering if they can be useful for this use-case.

__How to improve the accuracy for intent classification?__ Consider using pre-trained transformer-based language models from [huggingface model hub](https://huggingface.co/models). You can read on how to use them in [Language Models in Rasa](https://rasa.com/docs/rasa/components/#languagemodelfeaturizer).


This project has already got a lot of functionality. Our group tried to explore many functions that a conversational agent for teaching programming could have. Rather than thinking of completely different use-cases for the project, we encourage you to focus on *Programming Assistant* & *Answering Java Related Questions* and think of new ways to explore them.


## 6. Study Resources

- [Algorithm Whiteboard](https://youtu.be/wWNMST6t1TA): the goal of this playlist is to explain some of Rasa’s research results but also to explain their natural language processing algorithms in general.
- [Rasa Masterclass](https://youtu.be/rlAQWbhwqLA): weekly video series on building contextual AI assistants with Rasa tools.
- [Slides from the seminar](https://drive.google.com/drive/folders/1YG5g3fGkue-nLTzZi0QP-LciUZP7ewND?usp=sharing): introduction to Conversational-AI, Rasa, initial ideas for the project.
